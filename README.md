# RD53-A Emulator #


### FPGA emulator of the RD53A IC [CERN-RD53-PUB-15-001] ###


### Contributors ###
- Douglas Smith (smithd@uw.edu)
- Dustin Werran (werrand@uw.edu) 
- Lev Kurilenko
- Logan Adams
- Joe Mayer 


### Build Info ###
- Targets Xilinx KC705 Kintex FPGA Board 
- Vivado 2017.4
- Requires unisim, unimacro, unifast, and secureip
- Only XPR and XCI files provided

### Interface ###
- UART -- 115200 Baud (8 bits data, 1 stop bit)
- Use any serial connection software you prefer, but it has been tested with Tera Term [https://ttssh2.osdn.jp/index.html.en]
- Ensure you have the CP210x USB to UART Bridge VCP Drivers to communicate if using Xilinx KC-705 FPGA Dev Board [http://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers]
- Commands can be sent via the serial UART interface by sending the following letters
- 'o' = oneshot trigger (single trigger)
- 'c' = continuous trigger 
- 's' = stop continuous trigger
- 'd' = data command (chosen at random from LFSR; full command will complete with one 'd')

### Output Data ###
- 64B/66B SERDES Implementation TX/RX 

################################## Clock Constraints ##########################
create_clock -period 5.000 -waveform {0.000 2.500} [get_ports sysclk_in_p]

################################# Location constraints #####################

#### LOCATION CONSTRAINTS ARE XILINX KC705 BOARD SPECIFIC

#Reset input - GPIO_SW_N
set_property PACKAGE_PIN AA12    [get_ports rst]
set_property IOSTANDARD LVCMOS15 [get_ports rst]

#Sys/Rst Clk - On board 200MHz clock
set_property IOSTANDARD LVDS  [get_ports sysclk_in_n]
set_property PACKAGE_PIN AD12 [get_ports sysclk_in_p]
set_property IOSTANDARD LVDS  [get_ports sysclk_in_p]

#LED Ports - LEDs
set_property PACKAGE_PIN F16     [get_ports {led[7]} ]
set_property IOSTANDARD LVCMOS25 [get_ports {led[7]} ]
set_property PACKAGE_PIN E18     [get_ports {led[6]} ]
set_property IOSTANDARD LVCMOS25 [get_ports {led[6]} ]
set_property PACKAGE_PIN G19     [get_ports {led[5]} ]
set_property IOSTANDARD LVCMOS25 [get_ports {led[5]} ]
set_property PACKAGE_PIN AE26    [get_ports {led[4]} ]
set_property IOSTANDARD LVCMOS25 [get_ports {led[4]} ]
set_property PACKAGE_PIN AB9     [get_ports {led[3]} ]
set_property IOSTANDARD LVCMOS15 [get_ports {led[3]} ]
set_property PACKAGE_PIN AC9     [get_ports {led[2]} ]
set_property IOSTANDARD LVCMOS15 [get_ports {led[2]} ]
set_property PACKAGE_PIN AA8     [get_ports {led[1]} ]
set_property IOSTANDARD LVCMOS15 [get_ports {led[1]} ]
set_property PACKAGE_PIN AB8     [get_ports {led[0]} ]
set_property IOSTANDARD LVCMOS15 [get_ports {led[0]} ] 

#OFF-CHIP Ports
#GPIO_SW_W
set_property PACKAGE_PIN AC6     [get_ports trigger]
set_property IOSTANDARD LVCMOS15 [get_ports trigger]

#GPIO_SW_E
set_property PACKAGE_PIN AG5     [get_ports command]
set_property IOSTANDARD LVCMOS15 [get_ports command]

#SMA GPIO

set_property PACKAGE_PIN Y23    [get_ports dataout_p]
set_property IOSTANDARD LVDS_25 [get_ports dataout_p]
set_property PACKAGE_PIN Y24    [get_ports dataout_n]
set_property IOSTANDARD LVDS_25 [get_ports dataout_n]

#FMC 
set_property PACKAGE_PIN B29     [get_ports htl]
set_property IOSTANDARD LVCMOS25 [get_ports htl]
set_property PACKAGE_PIN C29     [get_ports clk40out]
set_property IOSTANDARD LVCMOS25 [get_ports clk40out]

#USB UART
set_property PACKAGE_PIN K24     [get_ports tx]
set_property IOSTANDARD LVCMOS25 [get_ports tx]
set_property PACKAGE_PIN M19     [get_ports rx]
set_property IOSTANDARD LVCMOS25 [get_ports rx]

# CHIP INPUT
#USER_SMA_CLOCK_P/J11
set_property PACKAGE_PIN L25    [get_ports {cmd_in_p[0]} ]
set_property IOSTANDARD LVDS_25 [get_ports {cmd_in_p[0]} ]
#USER_SMA_CLOCK_N/J12
set_property PACKAGE_PIN K25    [get_ports {cmd_in_n[0]} ]
set_property IOSTANDARD LVDS_25 [get_ports {cmd_in_n[0]} ]
#FMC_HPC_LA14_P
set_property PACKAGE_PIN B28    [get_ports {cmd_in_p[1]} ]
set_property IOSTANDARD LVDS_25 [get_ports {cmd_in_p[1]} ]
#FMC_HPC_LA14_N
set_property PACKAGE_PIN A28    [get_ports {cmd_in_n[1]} ]
set_property IOSTANDARD LVDS_25 [get_ports {cmd_in_n[1]} ]
#FMC_HPC_LA18_CC_P
set_property PACKAGE_PIN F21    [get_ports {cmd_in_p[2]} ]
set_property IOSTANDARD LVDS_25 [get_ports {cmd_in_p[2]} ]
#FMC_HPC_LA18_CC_N
set_property PACKAGE_PIN E21    [get_ports {cmd_in_n[2]} ]
set_property IOSTANDARD LVDS_25 [get_ports {cmd_in_n[2]} ]
#FMC_HPC_LA27_P
set_property PACKAGE_PIN C19    [get_ports {cmd_in_p[3]} ]
set_property IOSTANDARD LVDS_25 [get_ports {cmd_in_p[3]} ]
#FMC_HPC_LA27_N
set_property PACKAGE_PIN B19    [get_ports {cmd_in_n[3]} ]
set_property IOSTANDARD LVDS_25 [get_ports {cmd_in_n[3]} ]
#set_property PACKAGE_PIN K6  [get_ports {cmd_in_p[0]} ]
#set_property PACKAGE_PIN K5  [get_ports {cmd_in_n[0]} ]
#set_property PACKAGE_PIN C2  [get_ports {cmd_in_p[1]} ]
#set_property PACKAGE_PIN C3  [get_ports {cmd_in_n[1]} ]
#set_property PACKAGE_PIN A22 [get_ports {cmd_in_p[2]} ]
#set_property PACKAGE_PIN A23 [get_ports {cmd_in_n[2]} ]
#set_property PACKAGE_PIN A26 [get_ports {cmd_in_p[3]} ]
#set_property PACKAGE_PIN A27 [get_ports {cmd_in_n[3]} ]

# DEBUG
set_property PACKAGE_PIN AB25 [get_ports {debug[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug[0]}]
set_property PACKAGE_PIN AA25 [get_ports {debug[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug[1]}]
set_property PACKAGE_PIN AB28 [get_ports {debug[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug[2]}]
set_property PACKAGE_PIN AA27 [get_ports {debug[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug[3]}]


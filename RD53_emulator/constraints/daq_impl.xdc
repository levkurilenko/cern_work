# False clocking path(s) for implementation only

#set_false_path -from [get_pins pll_i/inst/mmcm_adv_inst/CLKOUT2] -to [get_pins buffer_i/U0/inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.gl0.wr/gwas.wsts/ram_full_fb_i_reg/D]
#set_false_path -from [get_pins pll_i/inst/mmcm_adv_inst/CLKOUT2] -to [get_pins {buffer_i/U0/inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gbm.gbmg.gbmga.ngecc.bmg/inst_blk_mem_gen/gnativebmg.native_blk_mem_gen/valid.cstr/ramloop[0].ram.r/prim_noinit.ram/DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram/ENBWREN}]

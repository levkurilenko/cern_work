// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Wed Oct 17 15:31:49 2018
// Host        : dhcp196-189.ee.washington.edu running 64-bit CentOS Linux release 7.5.1804 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/dsmith/Desktop/cern_work/RD53_emulator/RD53_Emulator/RD53_Emulation.runs/triggerTagFifo_synth_1/triggerTagFifo_stub.v
// Design      : triggerTagFifo
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v13_2_1,Vivado 2017.4" *)
module triggerTagFifo(rst, wr_clk, rd_clk, din, wr_en, rd_en, dout, full, 
  empty)
/* synthesis syn_black_box black_box_pad_pin="rst,wr_clk,rd_clk,din[4:0],wr_en,rd_en,dout[4:0],full,empty" */;
  input rst;
  input wr_clk;
  input rd_clk;
  input [4:0]din;
  input wr_en;
  input rd_en;
  output [4:0]dout;
  output full;
  output empty;
endmodule

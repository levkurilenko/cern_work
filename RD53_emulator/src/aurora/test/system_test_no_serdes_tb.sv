// Testbench: Gearbox + Scrambler + SERDES + Block Sync test
//
//                       Tx Gearbox            OSERDES              ISERDES            Rx Gearbox
//  +----------+        +----------+        +-----------+        +-----------+        +----------+        +----------+   2             2   +-------------+
//  |   Tx     |   66   |          |   32   |           |   8    |           |   32   |          |   66   |   Rx     | --/-->  sync  --/-->| Block Sync  |
//  |Scrambler | --/--> | 66 to 32 | --/--> |  32 to 8  | --/--> |  8 to 32  | --/--> | 32 to 66 | --/--> |   De     |   64                +-------------+
//  |          |        |          |        |           |        |           |        |          |        |Scrambler | --/-->  data
//  +----------+        +----------+        +-----------+        +-----------+        +----------+        +----------+
//

`timescale  1ns / 10fs

`define PROPER_HEADERS

module system_test_no_serdes_tb();

// Clocks
parameter half_clk40_period = 12.5;
parameter clk160_period = 6.25;
parameter clk640_period = 1.5625;

reg clk40;
reg clk160;
reg clk640;

// Reset
reg rst;
reg rst_tx, rst_rx;

// Gearbox Signals
wire [31:0] data32_tx_gb;
wire [65:0] data66_rx_gb;
wire        data_next;
wire        data_valid;

reg         gearbox_en_rx;
reg         gearbox_en_tx;
wire        gearbox_rdy_rx;
wire        gearbox_rdy_tx;

// Scrambler Signals
reg  [63:0] data_in;
reg  [1:0]  sync;
wire [65:0] data66_tx_scr;
wire        enable0;

// OSERDES Signals
reg         rst_serdes;
wire        data_out_p;
wire        data_out_n;
reg [7:0]   piso;
reg [1:0]   oserdes_cnt;
reg         serdes_cnt_init;

// ISERDES Signals
reg [31:0]  data32_iserdes;
reg [31:0]  data32_iserdes_latch;   // Just used for testing purposes
wire [7:0]  sipo;
reg [1:0]   iserdes_cnt;

// Descrambler Signals
wire [63:0] data64_rx_uns;
wire [1:0]  sync_out;
reg         enable1;

// Block Sync Signals
wire        blocksync_out;
wire        rxgearboxslip_out;
reg         gearbox_rdy_rx_r0;
reg         gearbox_rdy_rx_r1;

// Bitslip FSM Signals
wire        iserdes_slip;
wire        gearbox_slip;

// Miscellaneous Signals
reg         cnt;
reg         clkcnt = 0;
reg         shift0;
reg         shift1;

integer     data = 0;

// Output Files
integer     c_delta;

// Init
initial begin
    enable1         <= 1'b0;
    rst             <= 1'b0;
    rst_serdes      <= 1'b0;
    clk40           <= 1'b1;
    clk160          <= 1'b1;
    clk640          <= 1'b1;
    cnt             <= 1'b0;
    gearbox_en_rx   <= 1'b0;
    gearbox_en_tx   <= 1'b0;

    serdes_cnt_init <= 1'b1;
    oserdes_cnt     <= 2'd0;
    iserdes_cnt     <= 2'd0;        // THIS WAS CHOSEN TO BE THE PERFECT VALUE. NOT GENERIC
    
    //$monitor("%d, %h", $time, data64_rx_uns);
    //$monitor("%d, %h", $time, data66_rx_gb == 66'h1_c0ff_ee00_0000_0000_0000_0000_0000_0000);
    
    // Declare files for debug
    c_delta = $fopen("./system_test_single_serdes_files/counter_delta.txt","w");
    $fwrite(c_delta, "greater_cnt         time   tx   rx     delta_t  rx_buf_pos\n");
end

// 40 MHz clock
always #(half_clk40_period) begin
    clk40 <= ~clk40;
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// 640 MHz clock
always #(clk640_period/2) begin
    clk640 <= ~clk640;
end

// SERDES counters
always @(posedge clk160) begin
    cnt <= ~cnt;
    
    serdes_cnt_init <= 1'b0;
    
    if (serdes_cnt_init == 0) begin
        oserdes_cnt <= oserdes_cnt + 1;
        iserdes_cnt <= iserdes_cnt + 1;
    end
    piso <= data32_tx_gb[8*oserdes_cnt +: 8];
    data32_iserdes[8*iserdes_cnt +: 8] <= sipo;
end

// Delay Flops
always @(posedge clk40) begin
    shift0 <= data_valid;
    shift1 <= shift0;

    gearbox_rdy_rx_r0 <= gearbox_rdy_rx;
    gearbox_rdy_rx_r1 <= gearbox_rdy_rx_r0;
end

// Header Compiler Directives
always @(posedge clk40) begin
    clkcnt <= clkcnt + 1;
    if (clkcnt == 1'b0) begin
        `ifdef PROPER_HEADERS
            // Proper headers
            sync <= ($urandom%2 == 1'b1) ? 2'b10 : 2'b01;
        `else
            // Improper headers
            sync <= $urandom%4;
        `endif
    end
end

wire test_rxgb_data_valid;
assign test_rxgb_data_valid = ~clkcnt;

assign enable0 = ((cnt == 0)&&!rst);

// Main Data Driver
initial begin
    @(posedge clk40);
    rst     <= 1'b1;
    rst_tx  <= 1'b1;
    rst_rx  <= 1'b1;
    data_in <= 1'b0;
    @(posedge clk40);
    rst_serdes <= 1'b1;
    @(posedge clk40);
    enable1 <= 1'b1;
    data_in <= {{8{4'hA}}, {8{4'hB}}};
    @(posedge clk40);
    rst     <= 1'b0;
    rst_serdes <= 1'b0;
    gearbox_en_tx <= 1'b1;
    @(posedge clk40);
    data_in <= 64'b0;
    @(posedge clk40);
    gearbox_en_rx <= 1'b1;
    @(posedge clk40);
    //data_in <= {64{1'b1}};
    data_in <= {32'hc0ff_ee00, 32'h0000_0000};
    //for (int i=0; i<2_000_000; i=i+1) begin
    for (int i=0; i<10_000; i=i+1) begin
        repeat (2) @(posedge clk40);
        //data_in <= {32'hc0ff_ee00, data};
        //data <= data + 1;
    end
    data_in <= 64'hc0ff_ee00_c0ca_c01a;
    repeat (200) @(posedge clk40);
    repeat (25) @(posedge clk40);
    
    $fclose(c_delta);
    $stop;
end

reg [63:0] clock_cnt;
reg task_rxgearboxslip_out;
reg bitslip_pulse_flag;
reg [3:0] bitslip_cnt;
// Bitslip Process
initial begin
    bitslip_pulse_flag <= 1'b0;
    bitslip_cnt <= 4'h0;
    clock_cnt <= ~0;
    forever begin
        @(posedge clk160);
        clock_cnt <= clock_cnt + 1;
        bitslip_cnt <= bitslip_cnt + 1;
        task_rxgearboxslip_out <= 1'b0;
        if (bitslip_cnt == 8) begin
            bitslip_pulse_flag <= 1'b0;
            bitslip_cnt <= 0;
        end
        bitslip_pulse(100, bitslip_pulse_flag);
        bitslip_pulse(200, bitslip_pulse_flag);
        bitslip_pulse(300, bitslip_pulse_flag);
        bitslip_pulse(400, bitslip_pulse_flag);
        bitslip_pulse(500, bitslip_pulse_flag);
        bitslip_pulse(600, bitslip_pulse_flag);
        bitslip_pulse(700, bitslip_pulse_flag);
    end
end

task bitslip_pulse;
    //input [63:0] data_task;
    input [63:0] clk_cnt;
    input bitslip_pulse_flag_task;
    
    if ( (clock_cnt == clk_cnt) && (bitslip_pulse_flag_task == 0) ) begin
            task_rxgearboxslip_out <= 1'b1;
            bitslip_pulse_flag <= 1'b1;
            bitslip_cnt <= 0;
    end
endtask

reg [1:0] update_cnt = 2'b00;
reg start_cnt = 0;
always @(posedge clk40) begin
    if (rxgearboxslip_out) begin
        start_cnt <= 1'b1;
    end
    
    if (start_cnt) begin
        update_cnt <= update_cnt + 1;
        if (update_cnt == 3) begin
            update_cnt <= 2'b00;
            start_cnt <= 1'b0;
            if ((data66_rx_gb == {2'b10, 64'hc0ff_ee00_0000_0000})|| (data66_rx_gb == {2'b01, 64'hc0ff_ee00_0000_0000})) begin
                $fwrite(c_delta, "%s, %d, %d, %d, %d, %d, %h\n", (tx_gb.counter > rx_gb.counter) ? "tx" : "rx",
                $time, tx_gb.counter, rx_gb.counter, (tx_gb.counter > rx_gb.counter) ?
                (tx_gb.counter - rx_gb.counter) : (64 - rx_gb.counter + tx_gb.counter), rx_gb.buffer_pos, data66_rx_gb);
            end
            else begin
                $fwrite(c_delta, "%s, %d, %d, %d, %d, %d\n", (tx_gb.counter > rx_gb.counter) ? "tx" : "rx",
                $time, tx_gb.counter, rx_gb.counter, (tx_gb.counter > rx_gb.counter) ?
                (tx_gb.counter - rx_gb.counter) : (64 - rx_gb.counter + tx_gb.counter), rx_gb.buffer_pos);
            end
        end
    end
end

always @(posedge clk40) begin
    if ((data66_rx_gb == {2'b10, 64'hc0ff_ee00_0000_0000})|| (data66_rx_gb == {2'b01, 64'hc0ff_ee00_0000_0000})) begin
        $display("%d, %h", $time, data66_rx_gb);
    end
end

// always @(posedge clk40) begin
//     data32_iserdes_latch <= data32_iserdes;
// end
reg [31:0] data32_shift[10];

// THIS SHIFTS THE INCOMING DATA SO IT PERFECTLY
// ALIGNS WITH THE RX GEARBOX BUFFER UPDATE
//
// NOT GENERIC
always @(posedge clk160) begin
    data32_shift[0] <= data32_iserdes;
    data32_shift[1] <= data32_shift[0];
    
    // data32_shift[2] <= data32_shift[1];
    // data32_shift[3] <= data32_shift[2];
    // data32_shift[4] <= data32_shift[3];
    // data32_shift[5] <= data32_shift[4];
    // 
    // data32_shift[6] <= data32_shift[5];
    // data32_shift[7] <= data32_shift[6];
    // data32_shift[8] <= data32_shift[7];
    // data32_shift[9] <= data32_shift[8];
    
    data32_iserdes_latch <= data32_shift[1];
end

// Aurora Tx
scrambler scr (
    .data_in(data_in),
    .sync_info(sync),
    .enable(enable0&gearbox_rdy_tx),
    .clk(clk40),
    .rst(rst),
    .data_out(data66_tx_scr)
);

gearbox66to32 tx_gb (
    .rst(rst),
    .clk(clk40),
    //.data66(data66_tx_scr),
    .data66({sync, data_in}),
    .gearbox_en(gearbox_en_tx),
    .gearbox_rdy(gearbox_rdy_tx),
    .data32(data32_tx_gb),
    .data_next(data_next)
);

//OSERDES Interface
cmd_oserdes piso0_1280(
   .data_out_from_device(piso),
   .data_out_to_pins_p(data_out_p),
   .data_out_to_pins_n(data_out_n),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

//ISERDES Interface
cmd_iserdes i0 (
    .data_in_from_pins_p(data_out_p),
    .data_in_from_pins_n(data_out_n),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    //.bitslip(iserdes_slip),
    .bitslip(task_rxgearboxslip_out),
    .data_in_to_device(sipo)
);

// Aurora Rx
gearbox32to66 rx_gb (
    .rst(rst),
    .clk(clk40),
    //.data32(data32_iserdes),
    //.data32(data32_iserdes_latch),
    .data32(data32_tx_gb),
    
    .gearbox_en(gearbox_en_rx),
    .gearbox_rdy(gearbox_rdy_rx),
    
    //.gearbox_slip(gearbox_slip),
    .gearbox_slip(rxgearboxslip_out),
    
    .data66(data66_rx_gb),
    
    // TEST PURPOSES ONLY
    .data_valid_in(test_rxgb_data_valid),
    
    .data_valid(data_valid)
);

descrambler uns (
    .data_in(data66_rx_gb), 
    .sync_info(sync_out),
    .enable(!shift1&gearbox_rdy_rx),
    .clk(clk40),
    .rst(rst),
    .data_out(data64_rx_uns)
);

bitslip_fsm bs_fsm (
    .clk(clk160),
    .rst(rst),
    .blocksync(blocksync_out),
    .rxgearboxslip(rxgearboxslip_out),
    .iserdes_slip(iserdes_slip),
    .gearbox_slip(gearbox_slip)
);

block_sync #
(
    .SH_CNT_MAX(16'd64),
    .SH_INVALID_CNT_MAX(10'd16)
)
b_sync
(
    //.clk(clk160),
    .clk(clk40),
    .system_reset(rst),
    .blocksync_out(blocksync_out),
    .rxgearboxslip_out(rxgearboxslip_out),
    .rxheader_in(sync_out),
    .rxheadervalid_in(shift1&gearbox_rdy_rx)
);

endmodule

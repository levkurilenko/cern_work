onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top /io_buf_config_driver_tb/rst
add wave -noupdate -expand -group Top /io_buf_config_driver_tb/clk160
add wave -noupdate -expand -group Top /io_buf_config_driver_tb/io_config
add wave -noupdate -expand -group Top /io_buf_config_driver_tb/start
add wave -noupdate -expand -group Top -color Coral /io_buf_config_driver_tb/latch
add wave -noupdate -expand -group Top -color Coral /io_buf_config_driver_tb/clk_io
add wave -noupdate -expand -group Top -color Coral /io_buf_config_driver_tb/ser_in
add wave -noupdate -expand -group Top /io_buf_config_driver_tb/shift_reg
add wave -noupdate -expand -group Top /io_buf_config_driver_tb/output_reg
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/rst
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/clk160
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/io_config
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/start
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/latch
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/clk_io
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/ser_in
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold -expand -subitemconfig {{/io_buf_config_driver_tb/io_buf_config/latch_reg[3]} {-color Gold -height 15} {/io_buf_config_driver_tb/io_buf_config/latch_reg[2]} {-color Gold -height 15} {/io_buf_config_driver_tb/io_buf_config/latch_reg[1]} {-color Gold -height 15} {/io_buf_config_driver_tb/io_buf_config/latch_reg[0]} {-color Gold -height 15}} /io_buf_config_driver_tb/io_buf_config/latch_reg
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/clk_io_reg
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/ser_in_reg
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/clk40_cnt
add wave -noupdate -expand -group Top -expand -group {IO Buffer Config Driver} -color Gold /io_buf_config_driver_tb/io_buf_config/shift_in_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {828 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {6640 ns}

vlib work

vcom -work work ../lcd_driver.vhd

vlog -work work ./lcd_tb.sv

vsim -t 1fs -novopt lcd_tb

view signals
view wave

do wave_lcd.do

run -all
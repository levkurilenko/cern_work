// Testbench: Gearbox + Scrambler + SERDES + Block Sync test
//
//           OSERDES              ISERDES           
//        +-----------+        +-----------+        
//   32   |           |   8    |           |   32   
// --/--> | 32 to 4x8 | --/--> | 4x8 to 32 | --/--> 
//        |           |        |           |        
//        +-----------+        +-----------+        
//

`timescale  1ns / 10fs

module single_serdes_tb();

// Clocks
parameter half_clk40_period = 12.5;
parameter clk160_period = 6.25;
parameter clk640_period = 1.5625;

reg clk40;
reg clk160;
reg clk640;

// Reset
reg rst_serdes;

// OSERDES Signals
reg [31:0] data32;
wire data_out_p;
wire data_out_n;
reg [7:0]  piso, piso1, piso2, piso3;
reg [1:0] serdes_cnt;
//wire [7:0] piso0_rev, piso1_rev, piso2_rev, piso3_rev;

// ISERDES Signals
reg [31:0] data32_iserdes;
wire [7:0]  sipo, sipo1, sipo2, sipo3;
reg rxgearboxslip_out;
reg [1:0] serdes_cnt_rx;

integer data = 0;

//OSERDES Interface
cmd_oserdes piso0_1280(
   .data_out_from_device(piso),
   .data_out_to_pins_p(data_out_p),
   .data_out_to_pins_n(data_out_n),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

//ISERDES Interface
cmd_iserdes i0 (
    .data_in_from_pins_p(data_out_p),
    .data_in_from_pins_n(data_out_n),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    .bitslip(rxgearboxslip_out),
    //.bitslip(1'b0),
    .data_in_to_device(sipo)
);

reg cnt_init;

initial begin
    rst_serdes          <= 1'b0;
    clk40               <= 1'b1;
    clk160              <= 1'b1;
    clk640              <= 1'b1;
    data32              <= 32'h0000_0000;
    rxgearboxslip_out   <= 1'b0;
    
    serdes_cnt          <= 2'd0;
    serdes_cnt_rx       <= 2'd3;
    cnt_init            <= 1'b1;
end

// 40 MHz clock
always #(half_clk40_period) begin
    clk40 <= ~clk40;
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// 640 MHz clock
always #(clk640_period/2) begin
    clk640 <= ~clk640;
end

always @(posedge clk160) begin
    cnt_init <= 0;
    
    if (cnt_init == 0) begin
        serdes_cnt_rx <= serdes_cnt_rx + 1;
        serdes_cnt <= serdes_cnt + 1;
    end
    piso <= data32[8*serdes_cnt +: 8];
    data32_iserdes[8*serdes_cnt_rx +: 8] <= sipo;
end

// Data Driving Process
initial begin
    @(posedge clk40);
    data32 <= 1'b0;
    @(posedge clk40);
    rst_serdes <= 1'b1;
    @(posedge clk40);
    data32 <= {8{4'hB}};
    @(posedge clk40);
    rst_serdes <= 1'b0;
    @(posedge clk40);
    data32 <= 32'b0;
    @(posedge clk40);
    @(posedge clk40);
    data32 <= {32{1'b1}};
    data <= {32'hc0ff_ee00};
    for (int i=0; i<200; i=i+1) begin
        repeat (2) @(posedge clk40);
        data32 <= data;
        data <= data + 1;       
    end
    data32 <= 32'hc0ca_c01a;
    repeat (200) @(posedge clk40);
    repeat (25) @(posedge clk40);
    $stop;
end

reg bitslip_pulse_flag;
reg [3:0] bitslip_cnt;
// Bitslip Process
initial begin
    bitslip_pulse_flag <= 1'b0;
    bitslip_cnt <= 4'h0;
    forever begin
        @(posedge clk160);
        bitslip_cnt <= bitslip_cnt + 1;
        rxgearboxslip_out <= 1'b0;
        if (bitslip_cnt == 8) begin
            bitslip_pulse_flag <= 1'b0;
            bitslip_cnt <= 0;
        end
        bitslip_pulse(32'hc0ff_ee08, bitslip_pulse_flag);
        bitslip_pulse(32'hc0ff_ee10, bitslip_pulse_flag);
        bitslip_pulse(32'hc0ff_ee18, bitslip_pulse_flag);
        bitslip_pulse(32'hc0ff_ee20, bitslip_pulse_flag);
        bitslip_pulse(32'hc0ff_ee28, bitslip_pulse_flag);
        bitslip_pulse(32'hc0ff_ee30, bitslip_pulse_flag);
        bitslip_pulse(32'hc0ff_ee38, bitslip_pulse_flag);
    end
end

task bitslip_pulse;
    input [31:0] data_task;
    input bitslip_pulse_flag_task;
    
    if ( (data32 == data_task) && (bitslip_pulse_flag_task == 0) ) begin
            rxgearboxslip_out <= 1'b1;
            bitslip_pulse_flag <= 1'b1;
            bitslip_cnt <= 0;
    end
endtask

endmodule

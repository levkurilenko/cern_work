// Engineer: Lev Kurilenko
// Date: 9/5/2017
// Email: levkur@uw.edu

// Testbench: FIFO Test (First Word Fall Through)
// Description: Testing FIFO to confirm functionality for Channel
//              Bonding in Aurora


`timescale  1ns / 1fs

module fifo_test_tb_fwft();

parameter cb_char = 64'hc0ff_ee00_0000_0000;

// Reset
reg rst;

// Clocks
parameter half_clk40_period = 12.5;

reg clk40;

// Aurora Tx Signals
reg  [63:0] data_in[4];
reg  [1:0]  sync[4];

// FIFO Signals
wire [65:0] din[4];
reg  wr_en[4];
reg  rd_en[4];
wire [65:0] dout[4];
wire full[4];
wire empty[4];

// Channel Bonding Signals
reg [3:0] cb_detect;

// Assignment of input to FIFO
assign din[0][65:0] = {sync[0], data_in[0]};
assign din[1][65:0] = {sync[1], data_in[1]};
assign din[2][65:0] = {sync[2], data_in[2]};
assign din[3][65:0] = {sync[3], data_in[3]};

//===================
// Clock Generation
//===================

// 40 MHz clock
always #(half_clk40_period) begin
    clk40 <= ~clk40;
end

// Init
initial begin
    rst         <= 1'b0;
    clk40       <= 1'b1;
    cb_detect   <= 4'h0;    
    for (int i=0; i<4; i=i+1) begin
        wr_en[i]      <= 1'b0;
        rd_en[i]      <= 1'b0;
        data_in[i]    <= 64'h0000_0000_0000_0000;
        sync[i]       <= 2'b00;
    end
end

// Main Test
initial begin
    rst <= 1'b0;
    repeat (2) @(posedge clk40);
    rst <= 1'b1;
    repeat (10) @(posedge clk40);
    rst <= 1'b0;
    repeat (2) @(posedge clk40);
    for (int i=0; i<4; i=i+1) begin
        data_in[i] <= {32'hc0ff_ee00, 32'h0000_0000};
        sync[i] <= 2'b01;
    end
    
    @(posedge clk40);
    repeat (2) @(posedge clk40);
    for (int i=0; i<2000; i=i+1) begin
        @(posedge clk40);
        
        // Data Driver Loop
        for (int j=0; j<4; j=j+1) begin
            if (i%10 == 0) begin
                data_in[j] <= 64'hc0ff_ee00_0000_0000;
                sync[j] <= 2'b10;
            end
            else begin
                if (wr_en[j] == 1) begin
                    data_in[j] <= data_in[j] + 1;
                    sync[j] <= 2'b01;
                end
                else begin
                    data_in[j] <= data_in[j];
                    sync[j] <= 2'b01;
                end
            end
        end
        
        for (int j=0; j<4; j=j+1) begin
            // FIFO Write Logic
            if (full[j]) begin
                wr_en[j] = 1'b0;
            end
            else if (i%33 == 0) begin
                wr_en[j] = 1'b0;
            end
            else begin
                wr_en[j] = 1'b1;
            end
            
            if ((dout[j][65:0] == {2'b10, cb_char}) && (cb_detect != 4'hF)) begin
                cb_detect[j] = 1'b1;
            end
        end
    end
    
    // Finish Simulation
    for (int i=0; i<4; i=i+1) begin
        wr_en[i] <= 1'b0;
        rd_en[i] <= 1'b0;
    end
    
    repeat (25) @(posedge clk40);
    $stop;
end



always @(*) begin
    // Channel Bonding Loop
    for (int j=0; j<4; j=j+1) begin
        // FIFO Read Logic
        // Check if FIFO is empty
        if (empty[j]) begin
            // rd_en[j] <= 1'b0;
            if (cb_detect == 4'hF) begin
                for (int k=0; k<4; k=k+1) begin
                    //if (k == j) begin
                    //    rd_en[k] <= 1'b1;
                    //end
                    //else begin
                        rd_en[k] = 1'b0;
                    //end
                end
                break;
            end
            else begin
                rd_en[j] = 1'b0;
            end
        end
        else if ((dout[j][65:0] == {2'b10, cb_char}) && (cb_detect != 4'hF)) begin
            rd_en[j] = 1'b0;
        end
        else if ((cb_detect[j] == 1) && (cb_detect != 4'hF)) begin
            rd_en[j] = 1'b0;
        end
        else begin
            rd_en[j] = 1'b1;
        end
    end
end

reg [65:0] din_shift[4][8];
// Lane Data Shifting Block
always @(posedge clk40) begin
    for (int i=0; i<4; i=i+1) begin
        for (int j=0; j<8; j=j+1) begin
            if (j == 0) begin
                din_shift[i][j] <= din[i];
            end
            else begin
                din_shift[i][j] <= din_shift[i][j-1];
            end
        end
    end
end

reg wr_en_shift[4][8];
// Write Enable Shifting Block
always @(posedge clk40) begin
    for (int i=0; i<4; i=i+1) begin
        for (int j=0; j<8; j=j+1) begin
            if (j == 0) begin
                wr_en_shift[i][j] <= wr_en[i];
            end
            else begin
                wr_en_shift[i][j] <= wr_en_shift[i][j-1];
            end
        end
    end
end

//============================================================================
//                            Module Instantiation
//============================================================================

fifo_fwft fifo_0 (
  .clk(clk40),      // input  wire clk
  .rst(rst),        // input  wire rst
  .din(din[0]),        // input  wire [65:0] din
  //.din(din_shift[0][0]),        // input  wire [65:0] din
  .wr_en(wr_en[0]),    // input  wire wr_en
  .rd_en(rd_en[0]),    // input  wire rd_en
  .dout(dout[0]),      // output wire [65:0] dout
  .full(full[0]),      // output wire full
  .empty(empty[0])     // output wire empty
);

fifo_fwft fifo_1 (
  .clk(clk40),      // input  wire clk
  .rst(rst),        // input  wire rst
  //.din(din[1]),        // input  wire [65:0] din
  .din(din_shift[1][5]),        // input  wire [65:0] din
  //.wr_en(wr_en[1]),    // input  wire wr_en
  .wr_en(wr_en_shift[1][5]),    // input  wire wr_en
  .rd_en(rd_en[1]),    // input  wire rd_en
  .dout(dout[1]),      // output wire [65:0] dout
  .full(full[1]),      // output wire full
  .empty(empty[1])     // output wire empty
);

fifo_fwft fifo_2 (
  .clk(clk40),      // input  wire clk
  .rst(rst),        // input  wire rst
  //.din(din[2]),        // input  wire [65:0] din
  .din(din_shift[2][2]),        // input  wire [65:0] din
  //.wr_en(wr_en[2]),    // input  wire wr_en
  .wr_en(wr_en_shift[2][2]),    // input  wire wr_en
  .rd_en(rd_en[2]),    // input  wire rd_en
  .dout(dout[2]),      // output wire [65:0] dout
  .full(full[2]),      // output wire full
  .empty(empty[2])     // output wire empty
);

fifo_fwft fifo_3 (
  .clk(clk40),      // input  wire clk
  .rst(rst),        // input  wire rst
  //.din(din[3]),        // input  wire [65:0] din
  .din(din_shift[3][7]),        // input  wire [65:0] din
  //.wr_en(wr_en[3]),    // input  wire wr_en
  .wr_en(wr_en_shift[3][7]),    // input  wire wr_en
  .rd_en(rd_en[3]),    // input  wire rd_en
  .dout(dout[3]),      // output wire [65:0] dout
  .full(full[3]),      // output wire full
  .empty(empty[3])     // output wire empty
);

endmodule

vlib work

vlog -work work ../io_buf_config_driver.sv
vlog -work work ./io_buf_config_driver_tb.sv

vsim -t 1ns -novopt io_buf_config_driver_tb

view signals
view wave

do wave_io_buf_config_driver.do

run -all
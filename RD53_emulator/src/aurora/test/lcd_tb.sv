// Testbench: LCD Test

`timescale  1ns / 1fs

`define PROPER_HEADERS

module lcd_tb();

// Clocks
parameter half_clk40_period = 12.5;
parameter clk50_period = 20;
parameter clk160_period = 6.25;
parameter clk640_period = 1.5625;
parameter clk1280_period = 0.78125;

reg clk40;
reg clk50;
reg clk160;
reg clk640;
reg clk1280;

// Reset
reg rst;

// LCD Signals
wire [3:0]  SF_D;
wire        LCD_E;
wire        LCD_RS;
wire        LCD_RW;
wire        SF_CE0;
reg  [15:0] inv_data_cnt;

//===================
// Clock Generation
//===================

// 40 MHz clock
always #(half_clk40_period) begin
    clk40 <= ~clk40;
end

// 50 MHz clock
always #(clk50_period/2) begin
    clk50 <= ~clk50;
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// 640 MHz clock
always #(clk640_period/2) begin
    clk640 <= ~clk640;
end

// 1.28 GHz clock
always #(clk1280_period/2) begin
    clk1280 <= ~clk1280;
end

reg [63:0] tx_buffer;

// Init
initial begin
    rst             <= 1'b0;    
    clk40           <= 1'b1;
    clk50           <= 1'b1;
    clk160          <= 1'b1;
    clk640          <= 1'b1;
    clk1280         <= 1'b1;
    inv_data_cnt    <= 16'h0000;
end

// Main Data Driver
initial begin
    @(posedge clk50);
    rst         <= 1'b1;
    repeat (10) @(posedge clk50);
    rst         <= 1'b0;
    for (int i=0; i<20_000_000; i=i+1) begin
        @(posedge clk50);
        inv_data_cnt <= inv_data_cnt + 1;
    end
    // rst_gb_rx   <= 1'b1;
    // data_in     <= 1'b0;
    // @(posedge clk40);
    // rst_serdes  <= 1'b1; // SERDES RESET NEEDS TO BE ASSERTED SOME TIME AFTER GLOBAL RESET. OTHERWISE ERRORS PRESENT
    // @(posedge clk40);
    // data_in     <= {{8{4'hA}}, {8{4'hB}}};
    // @(posedge clk40);
    // rst         <= 1'b0;
    // rst_serdes  <= 1'b0;
    // @(posedge clk40);
    // data_in     <= 64'b0;
    // repeat (2) @(posedge clk40);
    // data_in <= {32'hc0ff_ee00, 32'h0000_0000};
    // for (int i=0; i<20_000; i=i+1) begin
    //     repeat (2) @(posedge clk40);
    // 
    //     // Uncomment top line if bit errors are desired. Used for testing bit error rate logic.
    //     //if (blocksync_out&gearbox_rdy_rx) begin
    //     if (blocksync_out&gearbox_rdy_tx) begin
    //         if ((i >= 10000)&&(i <= 15000)) begin
    //             data_in <= data_in;
    //         end
    //         else begin
    //             data_in <= data_in + 1;
    //         end
    //     end
    //     
    //     // Set what data iteration to deassert Rx Gearbox Reset
    //     if (i == 1500) begin
    //         rst_gb_rx <= 1'b0;
    //     end
    //     
    //     // if (i[0] == 1)
    //     //     data_in <= {32'hc0ff_ee00, 32'h0000_0000};
    //     // else
    //     //     data_in <= {32'hdeadbeef, 32'hc0cac01a};
    //     
    //     //data_in <= {32'hc0ff_ee00, data};
    //     //data <= data + 1;
    // end
    // data_in <= 64'hc0ff_ee00_c0ca_c01a;
    // repeat (200) @(posedge clk40);
    // repeat (25) @(posedge clk40);
    // 
    $stop;
end

lcd lcd_debug (
    .clk(clk50),
    .rst(rst),
    .SF_D(SF_D),
    .LCD_E(LCD_E),
    .LCD_RS(LCD_RS),
    .LCD_RW(LCD_RW),
    .SF_CE0(SF_CE0),
    .inv_data_cnt(inv_data_cnt)
);

endmodule

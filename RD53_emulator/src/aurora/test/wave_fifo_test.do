onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /fifo_test_tb/clk40
add wave -noupdate -expand /fifo_test_tb/data_in
add wave -noupdate -expand /fifo_test_tb/din
add wave -noupdate -expand /fifo_test_tb/dout
add wave -noupdate -expand /fifo_test_tb/rd_en
add wave -noupdate -expand /fifo_test_tb/wr_en
add wave -noupdate -expand /fifo_test_tb/cb_detect
add wave -noupdate -expand /fifo_test_tb/empty
add wave -noupdate /fifo_test_tb/full
add wave -noupdate /fifo_test_tb/half_clk40_period
add wave -noupdate /fifo_test_tb/rst
add wave -noupdate /fifo_test_tb/sync
add wave -noupdate /fifo_test_tb/wr_cnt
add wave -noupdate /fifo_test_tb/din_shift
add wave -noupdate /fifo_test_tb/wr_en_shift
add wave -noupdate -expand -group {Shifted FIFO wr_en} /fifo_test_tb/fifo_0/wr_en
add wave -noupdate -expand -group {Shifted FIFO wr_en} /fifo_test_tb/fifo_1/wr_en
add wave -noupdate -expand -group {Shifted FIFO wr_en} /fifo_test_tb/fifo_2/wr_en
add wave -noupdate -expand -group {Shifted FIFO wr_en} /fifo_test_tb/fifo_3/wr_en
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2230323529 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 170
configure wave -valuecolwidth 249
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {0 fs} {53576250 ps}

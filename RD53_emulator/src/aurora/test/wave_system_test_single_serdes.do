onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top /system_test_single_serdes_tb/clk40
add wave -noupdate -expand -group Top /system_test_single_serdes_tb/clk160
add wave -noupdate -expand -group Top /system_test_single_serdes_tb/clk640
add wave -noupdate -expand -group Top /system_test_single_serdes_tb/clkcnt
add wave -noupdate -expand -group Top /system_test_single_serdes_tb/sync
add wave -noupdate -expand -group Top /system_test_single_serdes_tb/tx_buffer
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_single_serdes_tb/scr/data_in
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_single_serdes_tb/scr/data_out
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_single_serdes_tb/scr/poly
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_single_serdes_tb/scr/scrambler
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_single_serdes_tb/scr/enable
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_single_serdes_tb/scr/rst
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold -radix binary /system_test_single_serdes_tb/scr/sync_info
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_single_serdes_tb/tx_gb/data66
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_single_serdes_tb/tx_gb/data32
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} -radix decimal /system_test_single_serdes_tb/tx_gb/counter
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_single_serdes_tb/tx_gb/data_next
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_single_serdes_tb/tx_gb/rst
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_single_serdes_tb/tx_gb/gearbox_rdy
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_single_serdes_tb/tx_gb/buffer_132
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /system_test_single_serdes_tb/piso0_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /system_test_single_serdes_tb/piso
add wave -noupdate -expand -group Top -expand -group OSERDES -group o0 -color {Orange Red} /system_test_single_serdes_tb/piso0_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -group o0 -color {Orange Red} /system_test_single_serdes_tb/piso0_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} /system_test_single_serdes_tb/i0/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} /system_test_single_serdes_tb/sipo
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} /system_test_single_serdes_tb/data32_iserdes
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} /system_test_single_serdes_tb/i0/bitslip
add wave -noupdate -expand -group Top -expand -group ISERDES -group i0 -color {Forest Green} /system_test_single_serdes_tb/i0/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -group i0 -color {Forest Green} /system_test_single_serdes_tb/i0/data_in_from_pins_n
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/iserdes_cnt
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/oserdes_cnt
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color Firebrick /system_test_single_serdes_tb/rx_gb/data32
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color Firebrick /system_test_single_serdes_tb/data32_iserdes
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/data32_iserdes_latch
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/rx_gb/data66
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} -radix unsigned /system_test_single_serdes_tb/rx_gb/counter
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/rx_gb/data_valid
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/rx_gb/rst
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/rx_gb/gearbox_rdy
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/rx_gb/gearbox_slip
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/rx_gb/buffer_128
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_single_serdes_tb/rx_gb/buffer_pos
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_single_serdes_tb/uns/data_in
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_single_serdes_tb/uns/data_out
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_single_serdes_tb/uns/poly
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_single_serdes_tb/uns/descrambler
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_single_serdes_tb/uns/enable
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_single_serdes_tb/uns/rst
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange -radix binary /system_test_single_serdes_tb/uns/sync_info
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_single_serdes_tb/b_sync/system_reset
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_single_serdes_tb/b_sync/blocksync_out
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_single_serdes_tb/b_sync/rxgearboxslip_out
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_single_serdes_tb/b_sync/rxheader_in
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_single_serdes_tb/b_sync/rxheadervalid_in
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} -radix unsigned /system_test_single_serdes_tb/b_sync/sync_header_count_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} -radix unsigned /system_test_single_serdes_tb/b_sync/sync_header_invalid_count_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/begin_r
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/BEGIN_R_ST
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/blocksync_out
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/clk
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/next_begin_c
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/next_sh_invalid_c
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/next_sh_valid_c
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/next_slip_c
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/next_sync_done_c
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/next_test_sh_c
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/rxgearboxslip_out
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/rxheader_in
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/rxheadervalid_in
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/SH_CNT_MAX
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/sh_count_equals_max_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/sh_invalid_cnt_equals_max_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/sh_invalid_cnt_equals_zero_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/SH_INVALID_CNT_MAX
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/sh_invalid_r
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/SH_INVALID_ST
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/sh_valid_r
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/SH_VALID_ST
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/slip_count_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/slip_done_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/slip_pulse_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/slip_r
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/SLIP_R_ST
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/sync_done_r
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/SYNC_DONE_R_ST
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/sync_found_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/sync_header_count_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/sync_header_invalid_count_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/system_reset
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/system_reset_r
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/system_reset_r2
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/test_sh_r
add wave -noupdate -expand -group Top -expand -group {Block Sync} -group Internal /system_test_single_serdes_tb/b_sync/TEST_SH_ST
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} /system_test_single_serdes_tb/bs_fsm/clk
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} /system_test_single_serdes_tb/bs_fsm/gearbox_slip
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} /system_test_single_serdes_tb/bs_fsm/iserdes_slip
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} /system_test_single_serdes_tb/bs_fsm/bitslip_state
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} /system_test_single_serdes_tb/bs_fsm/bitslip_state_next
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} /system_test_single_serdes_tb/bs_fsm/blocksync
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} -radix unsigned /system_test_single_serdes_tb/bs_fsm/gearbox_slip_cnt
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} -radix unsigned /system_test_single_serdes_tb/bs_fsm/iserdes_slip_cnt
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} /system_test_single_serdes_tb/bs_fsm/rst
add wave -noupdate -expand -group Top -expand -group {Bitslip FSM} -color {Lime Green} /system_test_single_serdes_tb/bs_fsm/rxgearboxslip
add wave -noupdate -expand -group Top -expand -group {Bit Error Rate} -color Goldenrod /system_test_single_serdes_tb/bit_err_cnt
add wave -noupdate -expand -group Top -expand -group {Bit Error Rate} -color Goldenrod /system_test_single_serdes_tb/inv_data_cnt
add wave -noupdate -expand -group Top -expand -group {Bit Error Rate} -color Goldenrod /system_test_single_serdes_tb/data64_latched
add wave -noupdate -expand -group Top -expand -group {Bit Error Rate} -color Goldenrod /system_test_single_serdes_tb/latched_true
add wave -noupdate -expand -group Top -expand -group {Bit Error Rate} -color Goldenrod /system_test_single_serdes_tb/data64_added
add wave -noupdate -expand -group Top -expand -group {Bit Error Rate} -color Goldenrod /system_test_single_serdes_tb/bit_err_cnt_next
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/byte
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/clk
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/cnt
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/count
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/digit
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/dstate
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/enable
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/idone
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/istate
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/LCD_E
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/LCD_RS
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/LCD_RW
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_cnt
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_count
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_digit
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_dstate
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_enable
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_idone
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_istate
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_regsel
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_selnibble
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_txcount
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/next_txdone
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/nibble
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/regsel
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/rst
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/selnibble
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/SF_CE0
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/SF_D
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/timer_15ms
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/timer_40us
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/timer_100us
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/timer_1640us
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/timer_4100us
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/txcount
add wave -noupdate -expand -group Top -expand -group LCD -color {Indian Red} /system_test_single_serdes_tb/lcd_debug/txdone
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {146328919173 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 233
configure wave -valuecolwidth 227
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 fs} {1056063750 ps}

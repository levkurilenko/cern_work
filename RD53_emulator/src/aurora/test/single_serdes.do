vlib work

vcom -work work ../cmd_oserdes_funcsim.vhdl

vcom -work work ../cmd_iserdes_funcsim.vhdl

vlog -work work ./single_serdes_tb.sv

vsim -t 10fs -novopt single_serdes_tb

view signals
view wave

do wave_single_serdes.do

run -all
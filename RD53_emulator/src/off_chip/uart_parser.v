module uart_parser(
    input clk40,
    input rst,
    input data_valid,
    input [7:0] data,
    
    output reg trigger,
    output reg command
);

reg trigger_mode; // Used to be output

reg [2:0] next_state;
reg [2:0] current_state;

reg [4:0] cont_counter;

parameter [7:0] one_trig = 8'h6F /* o */, data_cmd = 8'h64 /* d */, cont_trig = 8'h63 /* c */, stop_trig = 8'h73 /* s */;
parameter [2:0] idle = 3'b000, o_trigger_mode = 3'b001, c_trigger_mode = 3'b010, cmd_mode = 3'b011;

// Create FF
always @(posedge clk40) begin
    if(rst) begin
        current_state <= idle;
        cont_counter <= 5'b0;
    end else begin
        current_state <= next_state;
        cont_counter <= cont_counter + 1;
    end
end

// Next state logic
always @(*) begin
    case(current_state) 
        idle: begin
            if((data == one_trig) & (data_valid)) next_state = o_trigger_mode;
            else if((data == cont_trig) & (data_valid)) next_state = c_trigger_mode;
            else if((data == data_cmd) & (data_valid)) next_state = cmd_mode;
            else next_state = idle;
        end
        o_trigger_mode: begin
            next_state = idle;
        end
        c_trigger_mode: begin
            if((data == stop_trig) & (data_valid)) next_state = idle;
            else next_state = c_trigger_mode;
        end
        cmd_mode: begin
            next_state = idle;
        end
        default: begin
            next_state = idle;
        end
    endcase
end

// Output logic
always @(*) begin
    case(current_state) 
        idle: begin
            trigger = 1'b0;
            command = 1'b0;
            trigger_mode = 1'b0;
        end
        o_trigger_mode: begin
            trigger = 1'b1;
            command = 1'b0;
            trigger_mode = 1'b0;
        end
        c_trigger_mode: begin
            trigger = (cont_counter == 5'b11111); // Roughly 1 MHz for testing
            command = 1'b0;
            trigger_mode = 1'b1;
        end
        cmd_mode: begin
            trigger = 1'b0;
            command = 1'b1;
            trigger_mode = 1'b0;
        end
        default: begin
            trigger = 1'b0;
            command = 1'b0;
            trigger_mode = 1'b0;
        end
    endcase    
end

endmodule // uart_parser

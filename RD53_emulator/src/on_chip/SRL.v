// SR16 - 16 bit SR

// testing: srl_tb.v, srl16.do

//16-bit shift reg, parameterized to channel
// This register has some built-in bit-slip logic.
module SR16 #(parameter channel = 4'h0)(
   input  clk,
   input  rst,
   input  datain,
   output valid,
   output [15:0] dataout,
   output [3:0] shift_count_o
);

//parameter channel = 4'h0;

reg [16:0] shift_reg;
reg [ 3:0] shift_count;
reg        valid_i;

assign shift_count_o = shift_count;

//Shift Register
always @ (posedge clk or posedge rst) begin
   if (rst) begin
      shift_reg   <= 17'h00000;
      shift_count <= channel;
      valid_i     <= 1'b0;
   end
   else begin
      shift_reg   <= {shift_reg[15:0], datain};
      if (shift_count == 4'hf) begin 
         shift_count <= 4'h0;
         valid_i     <= 1'b1;
      end
      else  begin
         shift_count <= shift_count + 1;
         valid_i     <= 1'b0;
      end
   end
end

assign valid   = valid_i;
assign dataout = shift_reg[15:0];

endmodule

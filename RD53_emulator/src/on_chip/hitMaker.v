// When the input next_hit is raised, the next trigger value is processed
// and if the next trigger value is high, new data will be generated.


module hitMaker(
    input tClk, hClk, dClk, rst, emptyTT, writeT, calSig,
    input [3:0] triggerClump,
    input [4:0] triggerTag,
    input [31:0] seed1, seed2,
    input next_hit, update_output_i,
    output [63:0] hitData,
    output full, empty, hitData_empty//,
    //input trig_out
);
     
    reg calDone, doAStep, sawAnEmpty;
    wire step;
    reg [4:0] calibrating;
    wire [63:0] randomData, startingCounter, hitDataFIFOOut, maskedData;
    reg [1:0] startLoop;
    wire new_data;
    
    wire holdDataEmpty, holdDataFull;
    wire doWriteT;
    
    /*
    ilaHit TestingHits
    (.clk(dClk)
      ,.probe0(writeT)
      ,.probe1(triggerClump)
      ,.probe2(next_hit)
      ,.probe3(hitData)
      ,.probe4(maskedData)
      ,.probe5(step)
      ,.probe6(trig_out));*/
    
    assign startingCounter = {62'b0, startLoop[1:0]};
    //assign hitData = calDone ? maskedData : startingCounter;
    assign hitData = maskedData;
    
    //calibration, make it sixteen in row
    always @(posedge tClk or posedge rst) begin
        if (rst) begin
            calibrating = 1'b0;
            calDone = 1'b0;
            startLoop = 1'b0;
        end else if (calibrating < 16 & calSig) begin
            calibrating = calibrating + 5'b1;
            calDone = 1'b0;
            startLoop = startLoop + 2'b1;
        end else if (calibrating < 16) begin
            calibrating = 5'b0;
            calDone = 1'b0;
            startLoop = startLoop + 2'b1;
        end else begin
            calibrating = calibrating;
            calDone = 1'b1;
            startLoop = startLoop; 
        end      
    end
    
    assign doWriteT = writeT; //& calDone;
    
    //reverse the order of the trigger so the fifo extracts it in the correct order
    wire [3:0] iTriggerClump;
    assign iTriggerClump[0] = triggerClump[3];
    assign iTriggerClump[1] = triggerClump[2];
    assign iTriggerClump[2] = triggerClump[1];
    assign iTriggerClump[3] = triggerClump[0];

    // the triggerFifo
    triggerFifo triggered (
        .rst(emptyTT), 
        .wr_clk(tClk),
        .rd_clk(hClk), 
        .din(iTriggerClump), 
        .wr_en(doWriteT), 
        .rd_en(~empty), 
        .dout(step),
        .full(full), 
        .empty(empty)
    );
    
    //prevent the PRNG from continually generating new numbers if the TT empties
    always @(posedge hClk) begin
       if(rst) begin
            doAStep = 1'b0;
           sawAnEmpty = 1'b0;
       end else if(~empty & sawAnEmpty == 1'b1) begin
            doAStep = 1'b0;
            sawAnEmpty = 1'b0;
        end else if(~empty) begin
            doAStep = step;
            sawAnEmpty = 1'b0;
        end else if(sawAnEmpty == 1'b0) begin
            doAStep = step;
            sawAnEmpty = 1'b1;
        end else begin
            doAStep = 1'b0;
            sawAnEmpty = 1'b1;
        end
    end
       
    
    // the data
    updatedPRNG PRNG1 (
        .clk(hClk), 
        .reset(rst), 
        .load(1'b0), 
        .seed(seed1), 
        .step(doAStep), 
        .dout(randomData[31:0]),
        .new_data(new_data)
    );
    updatedPRNG PRNG2 (
        .clk(hClk), 
        .reset(rst), 
        .load(1'b0), 
        .seed(seed2), 
        .step(doAStep), 
        .dout(randomData[63:32]),
        .new_data()
    );
    
    //store the data
    hitDataFIFO hold_data (
        .rst(rst),
        .wr_clk(hClk),
        .rd_clk(dClk),
        .din(randomData),
        .wr_en(new_data),
        .rd_en(next_hit),
        .dout(hitDataFIFOOut),
        .full(holdDataFull),
        .empty(hitData_empty)
    );
    
    assign maskedData[63:0] = hitDataFIFOOut[63:0] & 64'h7F7F7F7F7F7F7F7F;  
    
endmodule

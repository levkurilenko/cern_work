// RD53_top
// Top level module to the RD53 chip emulator -- contains all internal logic

// testing: emulator_tb.v, emulator.do

module RD53_top (
   input        rst,
   input        clk40, clk80, clk160, clk640,
   input        ttc_data, button1,
   output [3:0] cmd_out_p, cmd_out_n,
   //output [3:0][7:0] cmd_out,
   output       trig_out,
      input [31:0] ila_cmdFull,
      input ila_data_read_o,
      input ilaActivate,
      output [3:0] OFB_o,
      output [3:0][7:0] data8_o,
      input [3:0] bitslip_i
   //output [15:0] command
);

wire word_valid;
wire [15:0] data_locked;
wire [63:0] frame_out [0:3];
wire [ 0:3] service_frame;
wire [ 1:0] sync      [0:3];
wire [ 3:0] data_next;


logic [3:0] clk160count, clk640count, clk80count, clk40count;
//logic [3:0][7:0] cmd_out; 
logic [15:0] command;
logic [31:0] commandsSent;

/*ila_full test
  
  (.clk(clk160)
  ,.probe0(clk160count)
  ,.probe1(4'h0000)
  ,.probe2(clk80count)
  ,.probe3(clk40count)
  ,.probe4(command)
  ,.probe5(word_valid));

ila_output test1
(.clk(clk160)
  ,.probe0(frame_out[0])
  ,.probe1(frame_out[1])
  ,.probe2(frame_out[2])
  ,.probe3(frame_out[3])
  ,.probe4(service_frame)
  ,.probe5(trig_out)
  ,.probe6(ila_data_read_o));*/

  
always @(posedge clk160)
  if (rst) 
      commandsSent <= 0;
  else if (ila_data_read_o)
      commandsSent <= commandsSent + 1;
  else
    commandsSent <= commandsSent;
  
  
always @(posedge clk160)
if (rst) 
    clk160count <= 0;
else
    clk160count <= clk160count + 1;
    
always @(posedge clk640)
    if (rst) 
        clk640count <= 0;
    else
        clk640count <= clk640count + 1;

always @(posedge clk80)
    if (rst) 
        clk80count <= 0;
    else
        clk80count <= clk80count + 1;

always @(posedge clk40)
    if (rst) 
        clk40count <= 0;
    else
        clk40count <= clk40count + 1;

// Parses TTC stream, locks to a channel
ttc_top ttc_decoder_i(
   .clk160(clk160),
   .rst(rst),
   .datain(ttc_data),
   .valid(word_valid),
   .data(data_locked),
   .ila_data_read_o(ila_data_read_o)
);

assign command = data_locked;
logic [15:0] data_locked_flipped;
assign data_locked_flipped[15] = data_locked[0];
assign data_locked_flipped[14] = data_locked[1];
assign data_locked_flipped[13] = data_locked[2];
assign data_locked_flipped[12] = data_locked[3];
assign data_locked_flipped[11] = data_locked[4];
assign data_locked_flipped[10] = data_locked[5];
assign data_locked_flipped[9] = data_locked[6];
assign data_locked_flipped[8] = data_locked[7];
assign data_locked_flipped[7] = data_locked[8];
assign data_locked_flipped[6] = data_locked[9];
assign data_locked_flipped[5] = data_locked[10];
assign data_locked_flipped[4] = data_locked[11];
assign data_locked_flipped[3] = data_locked[12];
assign data_locked_flipped[2] = data_locked[13];
assign data_locked_flipped[1] = data_locked[14];
assign data_locked_flipped[0] = data_locked[15];


// Prepare data from RD53 to be sent out
chip_output cout_i(
   .rst(rst),
   .clk160(clk160),
   .clk80(clk80),
   .clk40(clk40),
   .word_valid(word_valid),
   .data_in(data_locked),
   .chip_id(4'b0),          // Currently not implemented
   .data_next(|data_next),  // All four lines should always present same data
   .frame_out(frame_out),
   .service_frame(service_frame),
   .trig_out(trig_out),
   .fifo_full(),
   .TT_full(),
   .TT_empty(),
   .button(ilaActivate),
   .button1(button1)
);

assign sync[0] = service_frame[0] ? 2'b10 : 2'b01;
assign sync[1] = service_frame[1] ? 2'b10 : 2'b01;
assign sync[2] = service_frame[2] ? 2'b10 : 2'b01;
assign sync[3] = service_frame[3] ? 2'b10 : 2'b01;

aurora_tx_four_lane four_lane_tx_core(
    .rst(rst),
    .clk40(clk40),
    .clk160(clk160),
    .clk640(clk640),
    .data_in(frame_out),
    .sync(sync),
    .data_out_p(cmd_out_p),
    .data_out_n(cmd_out_n),
    //.data_out(cmd_out),
    .data_next(data_next),
    .ilaActivate(0),
    .OFB_o(OFB_o),
    .data8_o(data8_o),
    .bitslip_i(bitslip_i)
);

endmodule

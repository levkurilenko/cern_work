// When the input next_hit is raised, the next trigger value is processed
// and if the next trigger value is high, new data will be generated.


module hitMaker1(
    input tClk, hClk, dClk, rst, emptyTT, writeT, calSig,
    input [3:0] triggerClump, 
    input [4:0] triggerTag,
    input [31:0] seed1, seed2,
    input next_hit, update_output_i,
    output [63:0] hitData,
    output full, empty, hitData_empty
    //input trig_out
);
   
  reg calDone, doAStep, sawAnEmpty;
  wire step;
  reg [4:0] calibrating;
  logic [15:0] randomData, randomData1;
  logic [63:0]  startingCounter, hitDataFIFOOut, maskedData;
  reg [1:0] startLoop;
  wire new_data;
  
  wire holdDataEmpty, holdDataFull;
  wire doWriteT;
  
  
  assign doWriteT = writeT;
  
  //reverse the order of the trigger so the fifo extracts it in the correct order
  wire [3:0] iTriggerClump;
  assign iTriggerClump[0] = triggerClump[3];
  assign iTriggerClump[1] = triggerClump[2];
  assign iTriggerClump[2] = triggerClump[1];
  assign iTriggerClump[3] = triggerClump[0];
  
  
  reg doRead;
  // the triggerFifo
  triggerFifo triggered (
      .rst(emptyTT), 
      .wr_clk(tClk),
      .rd_clk(hClk), 
      .din(iTriggerClump), 
      .wr_en(doWriteT), 
      .rd_en(~empty), 
      .dout(step),
      .full(full), 
      .empty(empty)
  );
  
  wire [4:0] tagInfoIn, tagInfoOut;
 
  //assign tagInfoIn[24:20] = tagID;
  //assign tagInfoIn[19:15] = triggerTag;
  //assign tagInfoIn[14:0] = BCID;
  assign tagInfoIn[4:0] = triggerTag;
  
  
  triggerTagFifo triggeredTag (
      .rst(emptyTT), 
      .wr_clk(tClk),
      .rd_clk(hClk), 
      .din(tagInfoIn), 
      .wr_en(doWriteT), 
      .rd_en(doRead), 
      .dout(tagInfoOut),
      .full(), 
      .empty()
  );
  
  reg [4:0] tagID;
  reg [14:0] BCID;
  //counter that manages when tag info should be read from second fifo
  reg [2:0] tagCounter;
  always @(posedge hClk) begin
      if(rst) begin
          tagCounter <= 0;
          doRead <= 0;
      end else if(empty) begin
          tagCounter <= 0;
          doRead <= 0;
      end else if(tagCounter == 4 & ~empty) begin
          tagCounter <= 1;
          doRead <= 1;
      end else if(tagCounter == 0 & ~empty) begin
          tagCounter <= tagCounter + 1;
          doRead <= 1;
      end else if(~empty) begin
          tagCounter <= tagCounter + 1;
          doRead <= 0;
      end else begin
          tagCounter <= 0;
          doRead <= 0;
      end
  end
  
  //manage internal tag IDs
  always @(posedge hClk) begin
      if(rst) begin
          tagID <= 31;
      end else if(doAStep) begin
          tagID <= tagID + 1;
      end else begin
          tagID <= tagID;
      end
  end
  
  //BCID counter
  always @(posedge hClk) begin
      if(rst) begin
          BCID <= 0;
      end else begin
          BCID <= BCID + 1;
      end
  end
  
  //prevent the PRNG from continually generating new numbers if the TT empties
  always @(posedge hClk) begin
     if(rst) begin
          doAStep = 1'b0;
         sawAnEmpty = 1'b0;
     end else if(~empty & sawAnEmpty == 1'b1) begin
          doAStep = 1'b0;
          sawAnEmpty = 1'b0;
      end else if(~empty) begin
          doAStep = step;
          sawAnEmpty = 1'b0;
      end else if(sawAnEmpty == 1'b0) begin
          doAStep = step;
          sawAnEmpty = 1'b1;
      end else begin
          doAStep = 1'b0;
          sawAnEmpty = 1'b1;
      end
  end
  
  // the data
    //update behavior
    logic [3:0] active_point;
    reg [3:0] region, region1;
    logic update_hold;
    always @(posedge hClk) begin
      if(rst) begin
        active_point <= 0;
        update_hold <= 0;
      end else if(update_output_i == 1 & update_hold == 0) begin
        active_point <= active_point + 1;
        update_hold <= 1;
      end else if(update_output_i == 1 & update_hold == 1) begin
        active_point <= active_point;
        update_hold <= 1;
      end else begin
        active_point <= active_point;
        update_hold <= 0;
      end
    end
  
  always_comb begin
    if(region[3:0] == active_point)
      randomData = 16'haaaa;
    else
      randomData = 16'h0000;
  end
  
  always_comb begin
      if(region1[3:0] == active_point)
        randomData1 = 16'haaaa;
      else
        randomData1 = 16'h0000;
    end
    
  reg [5:0] col, row;
  //update row, col, and region
  always @(posedge hClk) begin
    if (rst) begin
      col <= 49;
      row <= 23;
      region <= 15;
    end
    else if (doAStep) begin
      if(region == 15 & col == 49) begin
        col <= 0;
        if(row == 23)
          row <= 0;
        else
          row <= row + 1;
      end
      else if (region == 15) begin
        col <= col + 1;
        row <= row;
      end
      else begin
        col <= col;
        row <= row;
      end
        
      region = region + 1;
    end
    else begin
      col <= col;
      row <= row;
      region <= region;
    end
  end
  
  reg [5:0] col1, row1;
    //update row, col, and region
    always @(posedge hClk) begin
      if (rst) begin
        col1 <= 0;
        row1 <= 0;
        region1 <= 0;
      end
      else if (doAStep) begin
        if(region1 == 15 & col1 == 49) begin
          col1 <= 0;
          if(row1 == 23)
            row1 <= 0;
          else
            row1 <= row1 + 1;
        end
        else if (region1 == 15) begin
          col1 <= col1 + 1;
          row1 <= row1;
        end
        else begin
          col1 <= col1;
          row1 <= row1;
        end
          
        region1 = region1 + 1;
      end
      else begin
        col1 <= col1;
        row1 <= row1;
        region1 <= region1;
      end
    end
  
  
  //delay some signals by one clock cycle
  reg [4:0] tagID_r;
  reg [4:0] tagInfoOut_r;
  reg [14:0] BCID_r;
  always @(posedge hClk) begin
    if (rst) begin
      tagID_r <= 0;
      tagInfoOut_r <= 0;
      BCID_r <= 0;
    end else begin
      tagID_r <= tagID;
      tagInfoOut_r <= tagInfoOut[4:0];
      BCID_r <= BCID;
    end
  end
  
  

  assign maskedData[63:57] = 7'b0000001; 
  assign maskedData[56:52] = tagID_r;
  assign maskedData[51:47] = tagInfoOut_r;
  assign maskedData[46:32] = BCID_r;
 
  //assign maskedData[63:58] = col1; 
  //assign maskedData[57:52] = row1;
  //assign maskedData[51:48] = region1;
  //assign maskedData[47:32] =     randomData1; 
  
  assign maskedData[31:26] = col; 
  assign maskedData[25:20] = row; 
  assign maskedData[19:16] = region; 
  assign maskedData[15:0] = randomData; 
  
  logic doAStep_r;
  always_ff @(posedge hClk) begin
    if (rst)
        doAStep_r <= 0;
    else
        doAStep_r <= doAStep;
  end
  
  //store the data
  hitDataFIFO hold_data (
      .rst(rst),
      .wr_clk(hClk),
      .rd_clk(dClk),
      .din(maskedData),
      .wr_en(doAStep_r),
      .rd_en(next_hit),
      .dout(hitData),
      .full(holdDataFull),
      .empty(hitData_empty)
  );
    
     
    
endmodule

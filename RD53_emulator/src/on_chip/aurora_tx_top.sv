// Engineer: Lev Kurilenko
// Date: 8/3/2017
// Module: Aurora Tx Top

`define SIM 0

module aurora_tx_top(
    input rst,
    input clk40,
    input clk160,
    input clk640,
    input [63:0] data_in,
    input [1:0]  sync,
    output data_out_p,
    output data_out_n,
    output data_next,
    input ilaActivate
);

localparam ber_char = 64'hB0B5_C0CA_C01A_CAFE;   // From Memory may need to be changed

// Tx Gearbox Signals
wire [31:0] data32_gb_tx;
wire        gearbox_rdy;

// Scrambler Signals
wire [65:0] data66_tx_scr;

// OSERDES Signals
/*
wire [7:0]  piso;
reg  [63:0] tx_buffer = 64'h0000_0000_0000_0000;
reg  [2:0]  tx_buf_cnt;*/
wire [3:0]  piso;
reg  [63:0] tx_buffer = 64'h0000_0000_0000_0000;
reg  [3:0]  tx_buf_cnt;

// VIO Signals
wire vio_rst;

ila_TXlanes theLanes
(.clk(clk160)
,.probe0(piso)
,.probe1(data32_gb_tx)
,.probe2(data66_tx_scr)
,.probe3(data_in)
,.probe4(ilaActivate));

// Serializer 32 to 8 bits
/*always @(posedge clk160 or posedge rst) begin
    if (rst) begin
        tx_buffer <= 64'h0000_0000_0000_0000;
        tx_buf_cnt <= 3'h0;
    end
    else begin
        tx_buf_cnt <= tx_buf_cnt + 1;
        
        if ((tx_buf_cnt == 0) || (tx_buf_cnt == 4)) begin
            tx_buffer <= {data32_gb_tx, tx_buffer[39:8]};
        end
        else begin
            tx_buffer <= {8'h00, tx_buffer >> 8};
        end
    end
end
assign piso = tx_buffer[7:0];*/


// Serializer 32 to 4 bits
always @(posedge clk160 or posedge rst) begin
    if (rst) begin
        tx_buffer <= 64'h0000_0000_0000_0000;
        tx_buf_cnt <= 4'h0;
    end
    else begin
        tx_buf_cnt <= tx_buf_cnt + 1;
        
        if ((tx_buf_cnt == 0) || (tx_buf_cnt == 8)) begin
            tx_buffer <= {data32_gb_tx, tx_buffer[35:4]};
        end
        else begin
            tx_buffer <= {4'h0, tx_buffer >> 4};
        end
    end
end
assign piso = tx_buffer[3:0];

//============================================================================
//                          Simulation
//============================================================================

// Pipeline rst for cmdoserdes using compilation directives
`ifdef SIM
    reg rst_r = 0;
    reg [4:0] rst_cnt = 3;
    reg rst_flag = 0;
    always @(posedge clk160) begin
        if (rst_cnt > 0) begin
            rst_cnt <= rst_cnt - 1'b1;
        end
        else if (rst_r & ~rst & ~rst_flag) begin
            rst_flag <= 1'b1;
            rst_r <= 1'b0;
            rst_cnt <= 31;
        end
        else begin
            if (rst_cnt > 0) begin
                rst_cnt <= rst_cnt - 1'b1;
                if (rst_cnt < 25)
                    rst_r <= 1'b1;
            end
            else
                rst_r <= rst;
        end
    end
`endif

//=========================================================================
//                              Aurora Tx
//=========================================================================

// Scrambler
scrambler scr (
    .clk(clk40),
    .rst(rst),
    .data_in(data_in),
    .sync_info(sync),
    .enable(data_next&gearbox_rdy),
    .data_out(data66_tx_scr)
);

// Gearbox
gearbox66to32 tx_gb (
    .rst(rst),
    .clk(clk40), 
    .data66(data66_tx_scr),
    //.data66({sync, data_in}),     // Use this to bypass Scrambler
    .gearbox_rdy(gearbox_rdy),
    .data32(data32_gb_tx),
    .data_next(data_next)
);
 logic serial;
 
 OBUFDS  buf_data (.O(data_out_p), .OB(data_out_n), .I(serial));
 
 OSERDESE2 #(.DATA_RATE_OQ("SDR"),
	       .DATA_RATE_TQ("SDR"),
	       .DATA_WIDTH(4),
	       .INIT_OQ(1'b1),
	       .INIT_TQ(1'b0),
	       .SERDES_MODE("MASTER"),
	       .SRVAL_OQ(1'b0),
	       .SRVAL_TQ(1'b0),
	       .TBYTE_CTL("FALSE"),
	       .TBYTE_SRC("FALSE"),
	       .TRISTATE_WIDTH(1)
	       )
   OSERDESE2_i
     (
      .OFB(),
      .OQ(serial),
      .SHIFTOUT1(),
      .SHIFTOUT2(),
      .TBYTEOUT(),
      .TFB(),
      .TQ(tq),
      .CLK(clk640),
      .CLKDIV(clk160),
      .D1(piso[0]),
      .D2(piso[1]),
      .D3(piso[2]),
      .D4(piso[3]),
      .D5(1'b0),
      .D6(1'b0),
      .D7(1'b0),
      .D8(1'b0),
      .OCE(1'b1),
      .RST(rst),
      .SHIFTIN1(1'b0),
      .SHIFTIN2(1'b0),
      .T1(1'b0),
      .T2(1'b0),
      .T3(1'b0),
      .T4(1'b0),
      .TBYTEIN(1'b0),
      .TCE(1'b0)
      );

//OSERDES Interface
/*cmd_oserdes piso0_1280(
   `ifdef SIM
        .io_reset(rst_r),
   `else
        .io_reset(rst),
   `endif
   .data_out_from_device(piso),
   .data_out_to_pins_p(data_out_p),
   .data_out_to_pins_n(data_out_n),
   .clk_in(clk640),
   .clk_div_in(clk160)
);*/

//============================================================================
//                          Debugging & Monitoring
//============================================================================

/* Debugging functions have been moved to four lane instantiation module
// ILA
ila_1 ila_slim (
    .clk(clk160),             // input wire clk
    .probe0(data_in),         // input wire [63:0] probe0   
    .probe1(gearbox_rdy)      // input wire [0:0] probe1 
);

vio_0 vio (
  .clk(clk40),
  .probe_out0(vio_rst)
);
*/

endmodule

// Engineer: Lev Kurilenko
// Date: 4/13/2018
// Module: Aurora Tx Top
// Description: Four Lane instantiation of the Aurora Tx core

module aurora_tx_four_lane(
    input clk40,
    input clk160,
    input clk640,
    input rst,
    input [63:0] data_in[4],
    input [1:0]  sync[4],
    output [3:0] data_out_p,
    output [3:0] data_out_n,
    output [3:0][7:0] data_out,
    output [3:0] data_next,
    output [3:0] OFB_o,
    input ilaActivate,
    output [3:0][7:0] data8_o,
    input [3:0] bitslip_i
);

//=========================================================================
//                          Aurora Tx Instantiation
//=========================================================================
genvar i;

logic [65:0] data_in_full [4];
assign data_in_full[3][65:64] = sync[3];
assign data_in_full[2][65:64] = sync[2];
assign data_in_full[1][65:64] = sync[1];
assign data_in_full[0][65:64] = sync[0];


assign data_in_full[3][63:0] = data_in[3];
assign data_in_full[2][63:0] = data_in[2];
assign data_in_full[1][63:0] = data_in[1];
assign data_in_full[0][63:0] = data_in[0];

generate
    for (i = 0 ; i <= 3 ; i = i+1)
        begin : tx_core
            aurora_tx_lane128 tx_lane (
                .rst_i(rst),
                .clk_i(clk160),
                .clkhigh_i(clk640),
                .data66tx_i(data_in_full[i]),
                //.data_out(data_out[i]),
                .dataout_p(data_out_p[i]),
                .dataout_n(data_out_n[i]),
                .read_o(data_next[i]),
                .OFB_o(OFB_o[i]),
                .data8_o(data8_o[i]),
                .bitslip_i(bitslip_i[i])
            );
    end
endgenerate

//============================================================================
//                          Debugging & Monitoring
//============================================================================

// // ILA
// ila_1 ila_slim (
//     .clk(clk160),             // input wire clk
//     .probe0(data_in),         // input wire [63:0] probe0   
//     .probe1(gearbox_rdy)      // input wire [0:0] probe1 
// );
// 
// vio_0 vio (
//   .clk(clk40),
//   .probe_out0(vio_rst)
// );

endmodule
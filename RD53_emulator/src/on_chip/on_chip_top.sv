// This module is the top level module for the on-chip electronics.
// That includes the RD53A emulators and the required PLLs.
// PLLs were separated from the emulators so that we could maximize
// the number of emulators stampable in the FPGA, rather than
// be limited by the number of PLLs.

// The clocks and TTC must be forwarded from the DAQ, but they are
// shared by all the emulators, so only one of each must be present
// for the system to work.
// Each emulator outputs 4 differential lanes of data.

// Testing: on_chip_top_tb.sv, on_chip_top.do

module on_chip_top #(
   parameter NUM_EMULATORS = 1
) (
   input        rst,// testClk,
   input        USER_SMA_CLOCK_P, USER_SMA_CLOCK_N,
   //input clk_160_i, clk_640_i, clk_80_i, clk_40_i,
   input        ttc_data_p [0:NUM_EMULATORS-1],
   input        ttc_data_n [0:NUM_EMULATORS-1],
   //input ttc_data [0:NUM_EMULATORS-1],
   output [3:0] cmd_out_p  [0:NUM_EMULATORS-1],
   output [3:0] cmd_out_n  [0:NUM_EMULATORS-1],
   //output [3:0][3:0] cmd_out  [0:NUM_EMULATORS-1],
   output       trig_out   [0:NUM_EMULATORS-1],  // May safely be commented out
   output [7:0] led,
   output [3:0] debug
   /*input [31:0] ila_cmdFull,
   input ila_data_read_o,
   input ilaActivate, button1,
   output [3:0] OFB_o,
   output [3:0][7:0] data8_o,
   input [3:0] bitslip_i*/
   //output ila_ttcData,
   //output ila_gotClk
);


logic clk40, clk80, clk160, clk640;  // Global clocks
logic ttc_data [0:NUM_EMULATORS-1];  // Single-ended TTC signal
logic mmcm_locked;                   // MMCM locked signal
logic rst_lock_halt;                 // High during reset or when MMCM not locked

// Pattern to indicate board is operating properly
assign led   = {6'b000000, mmcm_locked, ttc_data[0]};
assign debug = {2'b00,     mmcm_locked, ttc_data[0]};

// wait for the mmcm to lock before releasing submodules from reset
assign rst_lock_halt = rst | !mmcm_locked;

// Frequencies
// Internal clocks generated from incoming clk sent over SMA or VHDCI
clk_wiz_2 pll_fast(
   .clk_in1_p(USER_SMA_CLOCK_P),
   .clk_in1_n(USER_SMA_CLOCK_N),
   .clk_out1(clk640),
   //.clk_in1(clk_160_i),
   .clk_out2(clk160),
   .clk_out3(clk80),
   .clk_out4(clk40),
   .reset(rst),
   .locked(mmcm_locked)
);

//assign clk160 = clk_160_i;
//assign clk640 = clk_640_i;
//assign clk80 = clk_80_i;
//assign clk40 = clk_40_i;


// Turn differential TTC signal into single-ended
genvar i;
generate
   for (i = 0; i < NUM_EMULATORS; i++) begin: dif2single
      IBUFDS IBUFDS_i(
         .O(ttc_data[i]),
         .I(ttc_data_p[i]),
         .IB(ttc_data_n[i])
      );
   end
   //ttc_data_p = ttc_data;
endgenerate
//assign ttc_data_p[0] = ttc_data[0];
//assign ttc_data_n[0] =  ~ttc_data[0];

// Generate the emulators and spread the requisite signals between them
generate
   for (i = 0; i < NUM_EMULATORS; i++) begin: emulators
      RD53_top emulator (
         .rst(rst_lock_halt),
         .clk40(clk40), 
         .clk80(clk80), 
         .clk160(clk160), 
         .clk640(clk640),
         .ttc_data(ttc_data[i]),
         .cmd_out_p(cmd_out_p[i]), 
         .cmd_out_n(cmd_out_n[i]),
         .trig_out(trig_out[i]),
         .ila_cmdFull(32'b0),
         .ilaActivate(1'b0),
         .bitslip_i(1'b0),
         .button1(1'b0)
      );
   end
endgenerate

/*
ila_RD53 test
(.clk(testClk)
,.probe0(clk160)
,.probe1(ttc_data[0]));*/
//assign ila_ttcData = ttc_data[0];
//assign ila_gotClk = clk160;

endmodule
// Engineer: Lev Kurilenko
// Date: 8/7/2017
// Module: Aurora Rx Top
// 
// Changelog: 
// (Lev Kurilenko) 8/7/2017 - Created top module

module aurora_rx_top(
    input sysclk_in_p,
    input sysclk_in_n,
    input rst_in,
    
    input data_in_p,
    input data_in_n,
    
    // output LCD_DB4_LS,
    // output LCD_DB5_LS,
    // output LCD_DB6_LS,
    // output LCD_DB7_LS,
    // output LCD_E_LS,
    // output LCD_RS_LS,
    // output LCD_RW_LS,
    
    output USER_SMA_CLOCK_P,
    output USER_SMA_CLOCK_N
);

// Resets
wire rst;

// Clocks
wire clk40;
// wire clk50; // For LCD
wire clk160;
wire clk640;
wire clk200;
wire clk300;
wire clk400;
wire mmcm_locked;

// ISERDES Signals
reg  [31:0] data32_iserdes;
wire [7:0]  sipo;

// Rx Gearbox Signals
wire        gearbox_rdy_rx;
wire [65:0] data66_gb_rx;
wire        data_valid;

// Descrambler Signals
wire [1:0]  sync_out;
wire [63:0] data_out;

// Block Sync Signals
wire        blocksync_out;
wire        rxgearboxslip_out;

// Bit Error Rate Signals
wire [63:0] ber_cnt;
wire ber_sync;
wire sync_init;

// Bitslip FSM Signals
wire        iserdes_slip;
wire        gearbox_slip;

// VIO Signals
wire        vio_rst;

assign rst = !mmcm_locked;

// Data Reception (8 bits to 32 bits)
always @(posedge clk160) begin
    if (rst|vio_rst) begin
        data32_iserdes <= 32'h0000_0000;
    end
    else begin
        data32_iserdes[31:24] <= sipo;
        data32_iserdes[23:16] <= data32_iserdes[31:24];
        data32_iserdes[15:8]  <= data32_iserdes[23:16];
        data32_iserdes[7:0]   <= data32_iserdes[15:8];
    end
end

reg [31:0] data32_iserdes_r;
reg [31:0] data32_iserdes_r_r;

always @(posedge clk40) begin
    if (rst|vio_rst) begin
        data32_iserdes_r <= 32'h0000_0000;
        data32_iserdes_r_r <= 32'h0000_0000;
    end
    else begin
        data32_iserdes_r <= data32_iserdes;
        data32_iserdes_r_r <= data32_iserdes_r;
    end
end

//==========================
//  Clock Generation MMCM
//==========================
// Frequencies
// clk40:  40  MHz
// clk160: 160 MHz
// clk640: 640 MHz
// 
// clk_wiz_0 pll_fast(
//    .clk_in1_p(sysclk_in_p),
//    .clk_in1_n(sysclk_in_n),
//    .clk_out1(clk640),
//    .clk_out2(clk160),
//    .clk_out3(clk40),
//    .reset(rst_in),
//    .locked(mmcm_locked)
// );

// // Frequencies
// // clk40:  10  MHz
// // clk160: 40 MHz
// // clk640: 160 MHz
// // 
// // If this PLL is instantiated the clocks
// // will run at slower frequencies, despite
// // having names such as clk40, clk160, clk640.
// clk_wiz_1 pll_slow(
//    .clk_in1_p(sysclk_in_p),
//    .clk_in1_n(sysclk_in_n),
//    .clk_out1(clk640),
//    .clk_out2(clk160),
//    .clk_out3(clk40),
//    .clk_out4(clk200),
//    .clk_out5(clk50),
//    .reset(rst_in),
//    .locked(mmcm_locked)
// );
// 
// OBUFDS #(
//     .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//     .SLEW("FAST")           // Specify the output slew rate (Changed from "SLOW" [default])
// ) clk160_obufds (
//     .O(USER_SMA_CLOCK_P),   // Diff_p output (connect directly to top-level port)
//     .OB(USER_SMA_CLOCK_N),  // Diff_n output (connect directly to top-level port)
//     .I(clk200)              // Buffer input
// );

// Frequencies
// clk40:  10  MHz
// clk160: 40 MHz
// clk640: 160 MHz
// clk200: 200 MHz
// clk50:  50 MHZ
// Internal clocks generated from incoming clk sent over SMA
// clk_wiz_2 pll_inc_clk(
//    .clk_in1_p(USER_SMA_CLOCK_P),
//    .clk_in1_n(USER_SMA_CLOCK_N),
//    .clk_out1(clk640),
//    .clk_out2(clk160),
//    .clk_out3(clk40),
//    .reset(rst_in),
//    .locked(mmcm_locked)
// );

// Frequencies
// clk40:  39.0625  MHz
// clk160: 156.25 MHz
// clk640: 625 MHz
// clk200: 312.5 MHz
// clk50:  50 MHZ
// 
// If this PLL is instantiated the clocks
// will run at slower frequencies, despite
// having names such as clk40, clk160, clk640.
clk_wiz_3 pll_fast(
   .clk_in1_p(sysclk_in_p),
   .clk_in1_n(sysclk_in_n),
   .clk_out1(clk640),
   .clk_out2(clk160),
   .clk_out3(clk40),
   //.clk_out4(clk200),
   //.clk_out4(clk300),
   .clk_out4(clk400),
   //.clk_out5(clk50),
   .reset(rst_in),
   .locked(mmcm_locked)
);

OBUFDS #(
    .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
    .SLEW("FAST")           // Specify the output slew rate (Changed from "SLOW" [default])
) clk160_obufds (
    .O(USER_SMA_CLOCK_P),   // Diff_p output (connect directly to top-level port)
    .OB(USER_SMA_CLOCK_N),  // Diff_n output (connect directly to top-level port)
    .I(clk160)              // Buffer input
);

//reg tri_en;
//wire clk640buf;

//IOBUF data_out_p_buffer (
//    .O(clk640buf),         // Buffer output
//    .IO(clk640inout),    // Buffer inout port (connect directly to top-level port)
//    .I(clk640),             // Buffer input
//    .T(tri_en)                // 3-state enable input, high=input, low=output
//);

//always @(posedge clk160) begin
//    if (rst|vio_rst) begin
//        tri_en <= 1'b0;
//    end
//end

// // Frequencies
// // clk40:  20 MHz
// // clk160: 80 MHz
// // clk640: 320 MHz
// // clk200: 200 MHz
// // clk50:  50 MHZ
// // 
// // If this PLL is instantiated the clocks
// // will run at slower frequencies, despite
// // having names such as clk40, clk160, clk640.
// clk_wiz_4 pll_mid(
//    .clk_in1_p(sysclk_in_p),
//    .clk_in1_n(sysclk_in_n),
//    .clk_out1(clk640),
//    .clk_out2(clk160),
//    .clk_out3(clk40),
//    .clk_out4(clk200),
//    .clk_out5(clk50),
//    .reset(rst_in),
//    .locked(mmcm_locked)
// );
// 
// OBUFDS #(
//     .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//     .SLEW("FAST")           // Specify the output slew rate (Changed from "SLOW" [default])
// ) clk160_obufds (
//     .O(USER_SMA_CLOCK_P),   // Diff_p output (connect directly to top-level port)
//     .OB(USER_SMA_CLOCK_N),  // Diff_n output (connect directly to top-level port)
//     .I(clk200)              // Buffer input
// );

//===================
// Aurora Rx
//===================
// // ISERDES without IDELAYCTRL or IDELAYE2
// cmd_iserdes i0 (
//     .data_in_from_pins_p(data_in_p),
//     .data_in_from_pins_n(data_in_n),
//     .clk_in(clk640),
//     .clk_div_in(clk160),
//     .io_reset(rst|vio_rst),
//     .bitslip(iserdes_slip),
//     .data_in_to_device(sipo)
// );

// wire delay_locked;
// // ISERDES with IDELAYCTRL or IDELAYE2
// selectio_wiz_0 i0 (
//   .data_in_from_pins_p(data_in_p),
//   .data_in_from_pins_n(data_in_n),
//   .clk_in(clk640),
//   .clk_div_in(clk160),
//   .io_reset(rst|vio_rst),
//   .in_delay_reset(rst|vio_rst),
//   .in_delay_data_ce(1'b0),
//   .in_delay_data_inc(1'b0),
//   .ref_clock(clk200),
//   .delay_locked(delay_locked),
//   .bitslip(iserdes_slip),
//   .data_in_to_device(sipo)
// );

// ISERDES with Fixed Tap Value
// selectio_wiz_0 i0 (
//   .data_in_from_pins_p(data_in_p),
//   .data_in_from_pins_n(data_in_n),
//   .clk_in(clk640),
//   .clk_div_in(clk160),
//   .io_reset(rst|vio_rst),
//   .ref_clock(clk200),
//   .delay_locked(delay_locked),
//   .bitslip(iserdes_slip),
//   .data_in_to_device(sipo)
// );

/** OLD SERDES REPLACED BY XAPP 1017 IMPLEMENTATION
selectio_wiz_0 i0 (
  .data_in_from_pins_p(data_in_p),
  .data_in_from_pins_n(data_in_n),
  .clk_in(clk640),
  .clk_div_in(clk160),
  .io_reset(rst|vio_rst),

  .in_delay_reset(in_delay_reset),      // input wire in_delay_reset
  .in_delay_tap_in(tap_value),          // input wire [4 : 0] in_delay_tap_in
  .in_delay_tap_out(tap_out),           // output wire [4 : 0] in_delay_tap_out
  .in_delay_data_ce(delay_ce),          // input wire [0 : 0] in_delay_data_ce
  .in_delay_data_inc(delay_inc),        // input wire [0 : 0] in_delay_data_inc
  
  //.ref_clock(clk200),
  //.ref_clock(clk300),
  .ref_clock(clk400),
  .delay_locked(delay_locked),
  .bitslip(iserdes_slip),
  .data_in_to_device(sipo)
);
**/
wire rx_lckd;
wire [28:0] debug;

serdes_1_to_468_idelay_ddr #(
	.S			(8),				// Set the serdes factor (4, 6 or 8)
 	.HIGH_PERFORMANCE_MODE 	("TRUE"),
      	.D			(1),				// Number of data lines
      	.REF_FREQ		(400.0),			// Set idelay control reference frequency, 300 MHz shown
      	.CLKIN_PERIOD		(1.666),			// Set input clock period, 600 MHz shown
	.DATA_FORMAT 		("PER_CLOCK"))  		// PER_CLOCK or PER_CHANL data formatting
iserdes_inst (                      
	.clk160             (clk160),
    .clk640             (clk640),
	.datain_p     		(data_in_p),
	.datain_n     		(data_in_n),
	.enable_phase_detector	(1'b1),				// enable phase detector (active alignment) operation
	.enable_monitor		(1'b0),				// enables data eye monitoring
	.dcd_correct		(1'b0),				// enables clock duty cycle correction
	.rxclk    		(),
	.idelay_rdy		(idelay_rdy),
	.system_clk		(),
	.reset     		(rst|vio_rst),
	.rx_lckd		(rx_lckd),
	.bitslip  		(iserdes_slip),
	.rx_data		(sipo),
	.bit_rate_value		(16'h1200),			// required bit rate value in BCD (1200 Mbps shown)
	.bit_time_value		(),				// bit time value
	.eye_info		(),				// data eye monitor per line
	.m_delay_1hot		(),				// sample point monitor per line
	.debug			(debug)) ;				// debug bus

wire ref_clk_bufg;

(* IODELAY_GROUP = "xapp_idelay" *)
  IDELAYCTRL
    delayctrl (
     .RDY    (idelay_rdy),
     .REFCLK (ref_clk_bufg),
     .RST    (rst|vio_rst));

  BUFG
    ref_clock_bufg (
     .I (clk400),
     .O (ref_clk_bufg));

gearbox32to66 rx_gb (
    .rst(rst|vio_rst),
    .clk(clk40),
    .data32(data32_iserdes_r_r),
    .gearbox_rdy(gearbox_rdy_rx),
    .gearbox_slip(gearbox_slip),
    .data66(data66_gb_rx),
    .data_valid(data_valid)
);

descrambler uns (
    .clk(clk40),
    .rst(!blocksync_out|rst|vio_rst),
    .data_in(data66_gb_rx), 
    .sync_info(sync_out),
    .enable(blocksync_out&data_valid&gearbox_rdy_rx),
    .data_out(data_out)
);

block_sync # (
    .SH_CNT_MAX(16'd400),           // default: 64
    .SH_INVALID_CNT_MAX(10'd16)     // default: 16
)
b_sync (
    .clk(clk40),
    .system_reset(rst|vio_rst),
    .blocksync_out(blocksync_out),
    .rxgearboxslip_out(rxgearboxslip_out),
    .rxheader_in(sync_out),
    .rxheadervalid_in(data_valid&gearbox_rdy_rx)
);

bitslip_fsm bs_fsm (
    .clk(clk160),
    .rst(rst|vio_rst),
    .blocksync(blocksync_out),
    .rxgearboxslip(rxgearboxslip_out),
    .iserdes_slip(iserdes_slip),
    .gearbox_slip(gearbox_slip)
);

ber ber_inst(
    .rst(rst|vio_rst),
    .clk40(clk40),
    .blocksync_out(blocksync_out),
    .data_valid(data_valid),
    .gearbox_rdy_rx(gearbox_rdy_rx),
    .data66_gb_rx(data66_gb_rx),
    .data64_rx_uns(data64_rx_uns),
    .ber_cnt(ber_cnt),
    .ber_sync(ber_sync),
    .sync_init(sync_init)
);

//============================================================================
//                          Debugging & Monitoring
//============================================================================

// ILA
ila_1 ila_slim (
    .clk(clk160),
    .probe0(rst|vio_rst),
    .probe1(blocksync_out),
    .probe2(data_out),
    .probe3(ber_cnt),
    .probe4(rx_lckd),
    .probe5(sipo),
    .probe6(debug),
    .probe7(ber_sync),
    .probe8(sync_init)
);

//ila_0 ila (
//	.clk(clk160),                  // input wire clk

//    .probe0(rst),                 // input wire [0:0]  probe0  
//	.probe1(clk40),               // input wire [0:0]  probe1 
//	.probe2(clk160),              // input wire [0:0]  probe2 
//	.probe3(1'b0),                // input wire [0:0]  probe3 
//    .probe4(mmcm_locked),         // input wire [0:0]  probe4 
//	.probe5(gearbox_rdy_rx),      // input wire [0:0]  probe5 
//	.probe6(data_valid),          // input wire [0:0]  probe6 
//	.probe7(blocksync_out),       // input wire [0:0]  probe7 
//	.probe8(rxgearboxslip_out),   // input wire [0:0]  probe8 
//	.probe9(iserdes_slip),        // input wire [0:0]  probe9 
//	.probe10(gearbox_slip),       // input wire [0:0]  probe10 
//	.probe11(data32_iserdes),     // input wire [31:0] probe11 
//    .probe12(sipo),               // input wire [7:0]  probe12 
//	.probe13(data66_gb_rx),       // input wire [65:0] probe13 
//	.probe14(sync_out),           // input wire [1:0]  probe14 
//	.probe15(data_out),           // input wire [63:0] probe15
//	.probe16(bit_err_cnt),        // input wire [15:0] probe16 
//    .probe17(bit_err_cnt_next),   // input wire [15:0] probe17 
//    .probe18(inv_data_cnt),       // input wire [15:0] probe18 
//    .probe19(data64_latched),     // input wire [63:0] probe19 
//    .probe20(data64_added),       // input wire [63:0] probe20 
//    .probe21(latched_true)        // input wire [0:0]  probe21
//);

// VIO
vio_0 vio (
  .clk(clk160),                 // input wire clk
  .probe_out0(vio_rst),         // output wire [0 : 0] probe_out0
  .probe_out1(vio_tap_value),   // output wire [4 : 0] probe_out1
  .probe_out2(vio_tap_en),      // output wire [0 : 0] probe_out2
  .probe_out3(vio_tap_set)      // output wire [0 : 0] probe_out3
);

// wire SF_CE0;

// LCD
// lcd lcd_debug (
//     .clk(clk50),
//     .rst(rst),
//     .SF_D({LCD_DB7_LS, LCD_DB6_LS, LCD_DB5_LS, LCD_DB4_LS}),
//     .LCD_E(LCD_E_LS),
//     .LCD_RS(LCD_RS_LS),
//     .LCD_RW(LCD_RW_LS),
//     .SF_CE0(SF_CE0),
//     .inv_data_cnt(inv_data_cnt)
// );
endmodule

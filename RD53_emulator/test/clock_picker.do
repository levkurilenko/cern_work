vlib work

vlog -work work ../src/on_chip/clock_picker.v 

vlog -work work clock_picker_tb.v
 
vsim -t 1pS -novopt clock_picker_tb -L unisim -L unifast -L secureip

view signals
view wave

do wave_clock_picker.do

run -all

`timescale 1ns/1ps

module gearbox66to32_tb();

reg rst, clk;
reg [65:0] data66;
reg gearbox_en;

wire [31:0] data32;
wire data_next;
wire [65:0] data66_reverse;

gearbox66to32 uut (
    .rst(rst),
    .clk(clk),
    .data66(data66_reverse),
    .data32(data32),
    .data_next(data_next)
);

parameter half_clk160 = 3.125;

initial begin
    rst <= 1'b0;
    clk <= 1'b1;
    data66 <= 66'b0;
end

always #(half_clk160) begin
    clk <= ~clk;
end

// some simple patterns to get a general idea of functionality
initial begin
    @(posedge clk);
    rst <= 1'b1;
    @(posedge clk);
    rst <= 1'b0;
    repeat (2) @(posedge clk);
    data66 <= {{22{1'b1}}, {22{1'b0}}, {22{1'b1}}};
    repeat (2) @(posedge clk);
    data66 <= {2'b10, {4{16'h817B}}};
    repeat (2) @(posedge clk);
    data66 <= {{22{1'b0}}, {22{1'b1}}, {22{1'b0}}};
    repeat (6) @(posedge clk);
    $stop;
end

assign data66_reverse = {<<{data66}};

endmodule
`timescale 1ps/1ps

module tb_top_v2;

reg clksend;
reg clkin;
reg rstin;
reg datain;
reg clr_valid;
reg clk40, clk401;
reg [1:0] count40, count401;
integer counter;
integer other_counter;
reg [15:0] datareg, upnext;
wire [15:0] dataout;
wire valid;

always @(posedge clkin or posedge rstin) begin
   if (rstin) begin
      datain  <= 1'b0;
      datareg <= 16'h817E;
      upnext <= 16'hf0f0;
      counter <= 32'd0;
      other_counter <= 32'd0;
   end 
   else begin
      if (counter < 32'd15) begin
         datain  <= datareg[15];
         counter <= counter + 1;
         datareg <= {datareg[14:0],datareg[15]};
      end
      else begin
         other_counter <= other_counter + 1;
         datain  <= datareg[15];
         counter <= 32'd0;
         if (other_counter < 32'd16) begin
            datareg <= {datareg[14:0],datareg[15]};
         end
         else begin
            datareg <= upnext;
            upnext  <= upnext + 1;
         end
      end
   end
end 

initial begin
   clkin = 1'b0;
   forever #3124 clkin = ~clkin;
end

ttc_top chip(
   .clk160(clkin),
   .rst(rstin),
   .datain(datain),
   .valid(valid),
   .data(dataout)
);

initial
begin
rstin = 1'b1 ;
#12500
#1562
rstin = 1'b0 ;
//$stop
end 

endmodule

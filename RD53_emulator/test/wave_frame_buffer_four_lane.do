onerror {resume}
quietly virtual signal -install /frame_buffer_four_lane_tb/uut/frame_incrementer_i {/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame  } read_frame_rev001
quietly virtual function -install /frame_buffer_four_lane_tb/uut/frame_incrementer_i -env /frame_buffer_four_lane_tb { &{/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[0], /frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[1], /frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[2], /frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[3] }} read_frame_rev
quietly WaveActivateNextPane {} 0
add wave -noupdate /frame_buffer_four_lane_tb/rst
add wave -noupdate /frame_buffer_four_lane_tb/clk80
add wave -noupdate /frame_buffer_four_lane_tb/frame
add wave -noupdate /frame_buffer_four_lane_tb/service_frame
add wave -noupdate /frame_buffer_four_lane_tb/frame_valid
add wave -noupdate /frame_buffer_four_lane_tb/present_frame
add wave -noupdate /frame_buffer_four_lane_tb/frame_hold
add wave -noupdate -radix binary /frame_buffer_four_lane_tb/service_hold
add wave -noupdate -radix decimal /frame_buffer_four_lane_tb/i
add wave -noupdate -radix binary -childformat {{(3) -radix binary} {(2) -radix binary} {(1) -radix binary} {(0) -radix binary}} -subitemconfig {{/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[0]} {-radix binary} {/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[1]} {-radix binary} {/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[2]} {-radix binary} {/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[3]} {-radix binary}} /frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame_rev
add wave -noupdate -radix binary -childformat {{{/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[3]} -radix binary} {{/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[2]} -radix binary} {{/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[1]} -radix binary} {{/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[0]} -radix binary}} -subitemconfig {{/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[3]} {-height 15 -radix binary} {/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[2]} {-height 15 -radix binary} {/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[1]} {-height 15 -radix binary} {/frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame[0]} {-height 15 -radix binary}} /frame_buffer_four_lane_tb/uut/frame_incrementer_i/read_frame
add wave -noupdate -divider internals
add wave -noupdate {/frame_buffer_four_lane_tb/uut/buffers[0]/buffer/service_buf}
add wave -noupdate {/frame_buffer_four_lane_tb/uut/buffers[1]/buffer/service_buf}
add wave -noupdate {/frame_buffer_four_lane_tb/uut/buffers[2]/buffer/service_buf}
add wave -noupdate {/frame_buffer_four_lane_tb/uut/buffers[3]/buffer/service_buf}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {690210 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 137
configure wave -valuecolwidth 508
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {587966 ps} {719216 ps}

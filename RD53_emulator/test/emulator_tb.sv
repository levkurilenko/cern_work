`timescale 1ns/1fs

module emulator_tb;

// RD53A Emulator Signals
reg rst, ttc_data;

wire [3:0] cmd_out_p, cmd_out_n;
wire trig_out;
wire [7:0] led;  

// Aurora Rx Signals
wire [63:0] data_out[4];
wire [1:0]  sync_out[4];
wire [3:0]  blocksync_out;
wire [3:0]  gearbox_rdy_rx;
wire [3:0]  data_valid;

// Aurora Channel Bonding Signals
wire [63:0] data_out_cb[4];
wire [1:0]  sync_out_cb[4];
wire data_valid_cb;
wire channel_bonded;

// Aurora Tx Signals
wire [3:0] cmd_out_p_sim, cmd_out_n_sim;
wire [3:0] data_next;

// Clocks
parameter half_clk40_period = 12.5;
parameter halfclk200 = 2.5;
parameter clk80_period = 12.5;
parameter clk160_period = 6.25;
parameter clk400_period = 2.5;
parameter clk640_period = 1.5625;
parameter clk1280_period = 0.78125;

reg clk40;
reg clk80;
reg clk160;
reg clk400;
reg clk640;
reg clk1280;

initial begin
rst = 1'b0 ;
#(1*halfclk200)
rst = 1'b1;
#(40*halfclk200)
rst = 1'b0 ;
end

//===================
// Clock Generation
//===================

initial begin
    clk40   = 1'b1;
    clk80   = 1'b1;
    clk160  = 1'b1;
    clk400  = 1'b1;
    clk640  = 1'b1;
    clk1280 = 1'b1;
end

// 40 MHz clock
always #(half_clk40_period) begin
    clk40 <= ~clk40;
end

// 80 MHz clock
always #(clk80_period/2) begin
   clk80 <= ~clk80;
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// 400 MHz clock
always #(clk400_period/2) begin
    clk400 <= ~clk400;
end

// 640 MHz clock
always #(clk640_period/2) begin
    clk640 <= ~clk640;
end

// 1.28 GHz clock
always #(clk1280_period/2) begin
    clk1280 <= ~clk1280;
end

// initial begin
   // clk200 = 1'b0;
   // forever #(halfclk200) clk200 = ~clk200;
// end

//Turn into differential signals
// always @ (*) begin
   // clk200_n   <= !clk200;
   // clk200_p   <= clk200;
// end

// always @ (*) begin
   // clk160_n   <= !clk160;
   // clk160_p   <= clk160;
// end

//=========================================================================
//                      RD53A Emulator Instantiation
//=========================================================================

RD53_top Emulator(
   .rst(rst),
   .clk40(clk40),
   .clk80(clk80),
   .clk160(clk160),
   .clk640(clk640),
   .ttc_data(ttc_data),
   .cmd_out_p(cmd_out_p),
   .cmd_out_n(cmd_out_n),
   .trig_out(trig_out)
);


//=========================================================================
//              4 x Aurora Tx Instantiation for SIMULATION ONLY
//=========================================================================
aurora_tx_four_lane four_lane_tx_core(
    .rst(rst),
    .clk40(clk40),
    .clk160(clk160),
    .clk640(clk640),
    //.data_in(frame_out),
    .data_in({64'h7800_0000_0000_0040, 64'h7800_0000_0000_0040, 64'h7800_0000_0000_0040, 64'h7800_0000_0000_0040}),
    .sync({
        2'b10,
        2'b10,
        2'b10,
        2'b10
    }),
    .data_out_p(cmd_out_p_sim),
    .data_out_n(cmd_out_n_sim),
    .data_next(data_next)
);

//=========================================================================
//                      4 x Aurora Rx Instantiation
//=========================================================================
genvar i;

generate
    for (i = 0 ; i <= 3 ; i = i+1)
        begin : rx_core
            // Original WITHOUT XAPP
            aurora_rx_top rx_lane (
                .rst(rst),
                //.clk40(clk40),
                //.clk160(clk160),
                //.clk640(clk640),
                .clk40(Emulator.clk40),
                .clk160(Emulator.clk160),
                .clk640(Emulator.clk640),
                .data_in_p(cmd_out_p[i]),
                .data_in_n(cmd_out_n[i]),
                //.data_in_p(cmd_out_p_sim[i]),
                //.data_in_n(cmd_out_n_sim[i]),
                .blocksync_out(blocksync_out[i]),
                .gearbox_rdy(gearbox_rdy_rx[i]),
                .data_valid(data_valid[i]),
                .sync_out(sync_out[i]),
                .data_out(data_out[i])
            );
            
            // XAPP included
            // aurora_rx_top_xapp rx_lane (
            //     .rst(rst),
            //     .clk40(clk40),
            //     .clk160(clk160),
            //     .clk640(clk640),
            //     .clk400(clk400),
            //     .data_in_p(cmd_out_p[i]),
            //     .data_in_n(cmd_out_n[i]),
            //     .blocksync_out(blocksync_out[i]),
            //     .gearbox_rdy(gearbox_rdy_rx[i]),
            //     .data_valid(data_valid[i]),
            //     .sync_out(sync_out[i]),
            //     .data_out(data_out[i])
            // );
    end
endgenerate

// Aurora Channel Bonding
channel_bond cb (
    .rst(rst),
    .clk40(clk40),
    .data_in(data_out),
    //.data_in({data_out[0], 64'h7800_0000_0000_0042, data_out[2], data_out[3]}),
    .sync_in(sync_out),
    .blocksync_out(blocksync_out),
    .gearbox_rdy_rx(gearbox_rdy_rx),
    .data_valid(data_valid),
    .data_out_cb(data_out_cb),
    .sync_out_cb(sync_out_cb),
    .data_valid_cb(data_valid_cb),
    .channel_bonded(channel_bonded)
);


//=========================================================================
//                      Simulation Stimulus
//=========================================================================

parameter sync_pattern = 16'h817E;
integer counter;
integer other_counter;
reg [15:0] datareg;
reg [63:0] calibrate;
reg [127:0] upnext;

always @(posedge clk160 or posedge rst) begin
   if (rst) begin
      ttc_data <= 1'b0;
      datareg  <= sync_pattern; // For lock
      calibrate <= 48'h6363_6A_6A_6A_A6;  // To calibrate
      // rd_wr cmd, address to read from, some triggers
      //upnext <= 64'h65_65_6A_6A_2B_2D_6A_6A;
	  
	  ///////////////////////////////////////////////////////////
	  //                   Test Bit Flip                       //
	  ///////////////////////////////////////////////////////////
	  
	  // rd_wr cmd, address to read from, some triggers
      //upnext <= 64'h64_65_6A_6A_2B_2D_6A_6A; // BF warning
	  
	  //WRREG cmd  {WrReg,WrReg} {{ChipId[3:0],WrRegMode,Addr[8:4]} {Addr[3:0],Data[15:10]} {Data[9:0]}
	  //upnext <= 64'h66_66_6A_6A_71_6A_6A_6C;   // write 1 to reg 1, success
	  //upnext <= 64'h66_66_6A_6A_71_6A_6A_6D; // BF error, data
	  //upnext <= 64'h66_67_6A_6A_71_6A_6A_6C; // BF warning
	  
	  //sync 2*Addr[3:0]0, 2*addr[8:4] 2*trigger6
	  //upnext <= 64'h81_7E_6A_6A_6A_6A_36_36; // sync, correct triggers
	  //upnext <= 64'h81_7E_80_7E_81_7E_81_7E; // sync, BF warning
	  
	  // ECR BCR cmd
	  //upnext <= 64'h5A_5A_5A_5A_59_59_59_59; // BCR reset BCID counter
	  //upnext <= 64'h5A_5A_5A_5A_59_59_5A_5A; // BCR reset BCID counter
	  //upnext <= 64'h5A_5A_5A_5B_59_59_59_58; // ECR BCR, BF warning
	  
	  // ECR trigger reset
	  //upnext <= 64'h36_36_36_36_36_36_5A_5A; 
	  
	  // Global pulse
	  //upnext <= 64'h5C_5C_6A_6A_5D_5C_6A_6A; // BF warning
	  
	  //NOOP
	  //upnext <= 64'h5B_5B_5B_5B_5B_5B_5B_5B; // BF warning
	  
	  ///////////////////////////////////////////////////////////
	  //                 Test Command error                    //
	  ///////////////////////////////////////////////////////////
	  upnext <= 128'h66_66_6A_6A_6A_6A_6A_6A_65_65_6A_6A_2B_2D_6A_6A; // RdReg. send data, no correct symbol pair
	  //upnext <= 64'h6A_6A_6C_6C_6A_6A_6C_6C; // send only data without command
	  //upnext <= 64'h67_67_6A_6A_71_6A_6A_6C; // WrReg . send data, no correct symbol pair
	  //upnext <= 64'h80_7F_80_7F_80_7F_80_7F; // Sync mismatch. 
	  //upnext <= 64'h1B_1B_1B_1B_1B_1B_1B_1B; // ECR no correct symbol pair
	  //upnext <= 64'h18_18_18_18_18_18_18_18; // BCR no correct symbol pair
	  //upnext <= 64'h5D_5D_6A_6A_5C_5C_6A_6A; // Global pulse. send data, no correct symbol pair
	  //upnext <= 64'h7B_7B_7B_7B_7B_7B_7B_7B; // Noop, no correct symbol pair
	  
      counter  <= 32'd0;
      other_counter <= 32'd0;
   end 
   else begin
      if (counter < 32'd15) begin
         ttc_data <= datareg[15];
         counter  <= counter + 1;
         datareg  <= {datareg[14:0],datareg[15]};
      end
      else begin
         other_counter <= other_counter + 1;
         ttc_data <= datareg[15];
         counter  <= 32'd0;
         if (other_counter < 32'd45) begin
            datareg <= {datareg[14:0],datareg[15]};
         end
         else if (other_counter < 32'd48) begin
            datareg <= calibrate[47:32];
            calibrate <= {calibrate[31:0], calibrate[47:32]};
         end
         else begin
            datareg <= upnext[127:112];
            upnext  <= {upnext[111:0], upnext[127:112]};
         end
      end
   end
end

endmodule

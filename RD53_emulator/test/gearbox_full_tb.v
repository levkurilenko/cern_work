// Testbench: Gearbox to gearbox test
//
//  Tx Gearbox          Rx Gearbox
// +----------+        +----------+
// |          |   32   |          |   66
// | 66 to 32 | --/--> | 32 to 66 | --/-->
// |          |        |          |
// +----------+        +----------+
//

module gearbox_full_tb();

reg rst_tx, rst_rx;
reg clk;
reg [65:0] data66;

wire gearbox_rdy_tx;
wire gearbox_rdy_rx;

reg gearbox_en_tx;
reg gearbox_en_rx;

wire [65:0] data66_out;
wire [31:0] data32;
wire data_next;
wire data_valid;

integer i = 0;

gearbox66to32 tx_gb (
    .rst(rst_tx),
    .clk(clk),
    .data66(data66),
    .gearbox_en(gearbox_en_tx),
    .gearbox_rdy(gearbox_rdy_tx),
    .data32(data32),
    .data_next(data_next)
);

gearbox32to66 rx_gb (
    .rst(rst_rx),
    .clk(clk),
    .data32(data32),
    .gearbox_en(gearbox_en_rx),
    .gearbox_rdy(gearbox_rdy_rx),
    .data66(data66_out),
    .data_valid(data_valid)
);

parameter half_clk160 = 3.125;
integer d32, d66, d66o, run32, run66;

initial begin
    rst_tx <= 1'b0;
    rst_rx <= 1'b0;
    clk <= 1'b1;
    data66 <= 66'b0;
    
    gearbox_en_tx <= 1'b0;
    gearbox_en_rx <= 1'b0;
    
    // Declare files for debug
    d32 = $fopen("./gearbox_full_files/data32.txt","w");
    d66 = $fopen("./gearbox_full_files/data66.txt","w");
    d66o = $fopen("./gearbox_full_files/data66_out.txt","w");
    run32 = $fopen("./gearbox_full_files/running32.txt","w");
    run66 = $fopen("./gearbox_full_files/running66.txt","w");
end

always #(half_clk160) begin
    clk <= ~clk;
end

reg [1:0] clk_cnt = 0;

always @(posedge clk) begin
    //$display($time, ": %b", data32);
    $fwrite(d32, $time, ": %b\n", data32);
    $fwrite(run32, "%b", data32);
    $fwrite(run66, "%b", data66_out);
    
    clk_cnt <= clk_cnt + 1;
    if (clk_cnt[0] == 0) begin
        $fwrite(d66, $time, ": %b\n", data66);
        $fwrite(d66o, $time, ": %b\n", data66_out);
        $fwrite(d66o, $time, ": %b\n", rx_gb.buffer_128);
    end
end

initial begin
    @(posedge clk);
    rst_tx <= 1'b1;
    rst_rx <= 1'b1;
    data66 <= {2'b10, {8{4'hC}}, {8{4'hD}}};
    @(posedge clk);
    rst_tx <= 1'b0;
    gearbox_en_tx <= 1'b1;
    @(posedge clk);
    data66 <= {2'b10, {8{4'hA}}, {8{4'hB}}};
    //rst_rx <= 1'b0;
    //gearbox_en_rx <= 1'b1;
    @(posedge clk);
    rst_rx <= 1'b0;
    gearbox_en_rx <= 1'b1;
    @(posedge clk);
    data66 <= 66'b0;
    repeat (2) @(posedge clk);
    data66 <= {66{1'b1}};
    
    for (i = 0; i <200; i = i + 1) begin
        repeat (2) @(posedge clk);
        data66 <= {2'b11,32'haaaa_bbbb,i};
        
        
        //repeat (2) @(posedge clk);
        //data66 <= {2'b10, {8{4'hC}}, {8{4'hD}}};
        //repeat (2) @(posedge clk);
        //data66 <= {2'b10, {8{4'hA}}, {8{4'hB}}};
        //repeat (2) @(posedge clk);
        //data66 <= 66'b0;
        //repeat (2) @(posedge clk);
        //data66 <= {66{1'b1}};
    end
    
    
    for (i = 0; i <200; i = i + 1) begin
        repeat (2) @(posedge clk);
        data66 <= 66'ha_aaaa_bbbb_aaaa_bbbb_aaaa_bbbb_aaaa_bbbb;
        
        
        //repeat (2) @(posedge clk);
        //data66 <= {2'b10, {8{4'hC}}, {8{4'hD}}};
        //repeat (2) @(posedge clk);
        //data66 <= {2'b10, {8{4'hA}}, {8{4'hB}}};
        //repeat (2) @(posedge clk);
        //data66 <= 66'b0;
        //repeat (2) @(posedge clk);
        //data66 <= {66{1'b1}};
    end
    
    repeat (12) @(posedge clk);
    
    $fclose(d32);
    $fclose(d66);
    $fclose(d66o);
    $fclose(run32);
    $fclose(run66);
    $stop;
end

endmodule
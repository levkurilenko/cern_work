vlib work

vlog -work work ../src/on_chip/gearbox_66_to_32.sv

vlog -work work ../src/off_chip/gearbox_32_to_66.v

vlog -work work ./gearbox_full_tb.v

vsim -t 1ps -novopt gearbox_full_tb

view signals
view wave

do wave_gearbox_full.do

run -all
vlib work

vlog -work work ../src/on_chip/shift_align.v
vlog -work work shiftalign_tb.v

vsim -t 1pS -novopt shiftalign_tb

view signals
view wave

do wave_shiftalign.do

run 700ns

onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top /serdes_tb/clk160
add wave -noupdate -expand -group Top /serdes_tb/clk640
add wave -noupdate -expand -group Top /serdes_tb/rxgearboxslip_out
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /serdes_tb/piso0_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /serdes_tb/piso1_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /serdes_tb/piso2_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /serdes_tb/piso3_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /serdes_tb/data32
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} -radix binary /serdes_tb/piso0
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} -radix binary /serdes_tb/piso1
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} -radix binary /serdes_tb/piso2
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} -radix binary /serdes_tb/piso3
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o0 -color {Orange Red} /serdes_tb/piso0_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o0 -color {Orange Red} /serdes_tb/piso0_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o1 -color {Orange Red} /serdes_tb/piso1_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o1 -color {Orange Red} /serdes_tb/piso1_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o2 -color {Orange Red} /serdes_tb/piso2_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o2 -color {Orange Red} /serdes_tb/piso2_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o3 -color {Orange Red} /serdes_tb/piso3_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o3 -color {Orange Red} /serdes_tb/piso3_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold /serdes_tb/i0/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold /serdes_tb/i1/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold /serdes_tb/i2/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold /serdes_tb/i3/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold /serdes_tb/data32_iserdes
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold -radix binary /serdes_tb/sipo0
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold -radix binary /serdes_tb/sipo1
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold -radix binary /serdes_tb/sipo2
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold -radix binary /serdes_tb/sipo3
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i0 -color Gold /serdes_tb/i0/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i0 -color Gold /serdes_tb/i0/data_in_from_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i1 -color Gold /serdes_tb/i1/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i1 -color Gold /serdes_tb/i1/data_in_from_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i2 -color Gold /serdes_tb/i2/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i2 -color Gold /serdes_tb/i2/data_in_from_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i3 -color Gold /serdes_tb/i3/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i3 -color Gold /serdes_tb/i3/data_in_from_pins_n
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7029 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 233
configure wave -valuecolwidth 227
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4140938 ps}

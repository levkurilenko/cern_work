`timescale 1ns/1ps

module gearbox32to66_tb();

reg rst, clk;
reg [31:0] data32;
reg gearbox_en;

wire [65:0] data66;
wire data_valid;
wire gearbox_rdy;

integer data;
integer i;

gearbox32to66 uut (
    .rst(rst),
    .clk(clk),
    .data32(data32),
    .data66(data66),
    .gearbox_en(gearbox_en),
    .gearbox_rdy(gearbox_rdy),
    .data_valid(data_valid)
);

parameter half_clk160 = 3.125;

initial begin
    rst <= 1'b0;
    clk <= 1'b1;
    data32 <= 32'b0;
    gearbox_en <= 1'b0;
    i <= 0;
    data <= 0;
end

always #(half_clk160) begin
    clk <= ~clk;
end

// some simple patterns to get a general idea of functionality
initial begin
    @(posedge clk);
    rst <= 1'b1;
    data32 <= {{4{4'hA}}, {4{4'hB}}};
    @(posedge clk);
    data32 <= {{4{4'hC}}, {4{4'hD}}};
    rst <= 1'b0;
    @(posedge clk);
    data32 <= {{16{1'b1}}, {16{1'b0}}};
    @(posedge clk);
    data32 <= 32'hF0F0F0F0;
    @(posedge clk);
    data32 <= {2{16'hAAAA}};
    @(posedge clk);
    data32 <= 32'hFF00FF00;
    @(posedge clk);
    data32 <= {{16{1'b0}}, {16{1'b1}}};
    gearbox_en <= 1;
    @(posedge clk);
    data32 <= 32'hF0F0F0F0;
    @(posedge clk);
    data32 <= {2{16'hAAAA}};
    @(posedge clk);
    data32 <= 32'hFF00FF00;
    @(posedge clk);
    data32 <= {{16{1'b0}}, {16{1'b1}}};
    for (i = 0; i < 80; i = i + 1) begin
        repeat (2) @(posedge clk);
        gearbox_en <= 1;
        //data66 <= {{2'b01}, $urandom(), $urandom()};
        data32 <= data;
        if (gearbox_rdy) begin
            data = data + 1;
        end
    end
    
    $stop;
end

endmodule

vlib work

vlog -work work ../src/aurora/scrambler.v
#vlog -work work ../src/scrambler_64b_58_39_1.sv
vlog -work work ../src/aurora/descrambler.v
vlog -work work scrambler_tb.sv

vsim -t 1pS -novopt scrambler_tb

view signals
view wave

do wave_scrambler.do

run 300
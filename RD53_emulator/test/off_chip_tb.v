`timescale 1ps/1ps 

module off_chip_tb;

parameter halfclk40 = 12500;
parameter halfclk160 = 3125;
parameter halfclk200 = 2500; 

reg rst, clk40, clk160, clk200, clk200_p, clk200_n, trig, cmd; 
wire dataout_p, dataout_n;
wire htl, clk40_out;
reg [3:0] shift_in;
wire [7:0] led;

initial begin
rst = 1'b0 ;
#(1*halfclk160)
rst = 1'b1;
#(7*halfclk160)
rst = 1'b0 ;
end

initial begin
   clk40 = 1'b0;
   forever #(halfclk40) clk40 = ~clk40;
end


initial begin
   clk200 = 1'b0;
   forever #(halfclk200) clk200 = ~clk200;
end
   
   
initial begin
   clk160 = 1'b0;
   forever #(halfclk160) clk160 = ~clk160;
end

//Turn into differential signals
always @ (*) begin
   clk200_n   <= !clk200;
   clk200_p   <= clk200;
end

off_chip_top nchip(
   .rst(rst),
   .sysclk_in_p(clk200_p),
   .sysclk_in_n(clk200_n),
   .trigger(trig),
   .command(cmd),
   .clk40out(clk40_out),
   .htl(htl),
   .dataout_n(dataout_n),
   .dataout_p(dataout_p),
   .led(led),
   .cmd_in_p(),
   .cmd_in_n(),
   .rx(),
   .tx(),
   .debug()
);

always @ (posedge clk40 or posedge rst) begin
   if (rst) begin
      trig     <= 1'b0;
      cmd      <= 1'b0;
   end
   else begin
      if (^shift_in) begin
         trig <= 1'b1;
         cmd  <= 1'b1;
      end
      else begin
         trig <= 1'b0;
         cmd  <= 1'b0;
      end
   end
end

always @ (posedge clk160 or posedge rst) begin
   if (rst) begin
      shift_in <= 4'h0;
   end
   else begin
      shift_in <= {shift_in[2:0], dataout_p};
   end
end

endmodule

vlib work

vlog -work work ../src/aurora_xilinx.v

vlog -work work -sv ../src/on_chip/rd53_tx.v
vlog -work work ../src/on_chip/gearbox_66_to_32.v
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/pre_gearbox_fifo/pre_gearbox_fifo_sim_netlist.vhdl

vlog -work work ../src/off_chip/rd53_rx.v
vlog -work work ../src/off_chip/gearbox_32_to_66.v
vcom -work work ../RD53_DAQ/RD53_Emulation.srcs/sources_1/ip/cmd_iserdes/cmd_iserdes_sim_netlist.vhdl

vlog -work work rd53_txrx_tb.v

vsim -t 1ps -novopt rd53_txrx_tb -L unisim -L unifast -L unimacro -L secureip

view signals
view wave

do wave_rd53_txrx.do

run 1 us

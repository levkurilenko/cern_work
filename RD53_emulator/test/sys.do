vlib work

#OFF_CHIP
vlog -work work ../src/off_chip/LFSR.v
vcom -work work ../RD53_DAQ/RD53_Emulation.srcs/sources_1/ip/cmd_fifo/cmd_fifo_sim_netlist.vhdl
vlog -work work ../src/off_chip/cmd_top.v
vlog -work work ../src/off_chip/sync_timer.v
vlog -work work ../src/off_chip/triggerunit.v
vlog -work work ../src/off_chip/SER.v
vcom -work work ../RD53_DAQ/RD53_Emulation.srcs/sources_1/ip/off_chip_pll/off_chip_pll_sim_netlist.vhdl
vlog -work work ../src/off_chip/uart.v
vlog -work work ../src/off_chip/uart_parser.v
vcom -work work ../RD53_DAQ/RD53_Emulation.srcs/sources_1/ip/cmd_iserdes/cmd_iserdes_sim_netlist.vhdl
vlog -work work ../src/off_chip/rd53_rx.v
vlog -work work ../src/off_chip/off_chip_top.v

#ON_CHIP
vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ../src/on_chip/hitMaker.v
vlog -work work ../src/on_chip/SRL.v
vlog -work work ../src/on_chip/shift_align.v
vlog -work work ../src/on_chip/clock_picker.v
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/shift_reg/shift_reg_funcsim.vhdl
vlog -work work ../src/on_chip/ttc_top.v 
vlog -work work ../src/on_chip/trigger_counter.v
vlog -work work ../src/on_chip/command_process.v
vlog -work work ../src/on_chip/hamming_generator.v
vlog -work work ../src/on_chip/command_out.v
vlog -work work ../src/on_chip/chip_output.v
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes_sim_netlist.vhdl
vlog -work work ../src/on_chip/rd53_tx.v
vlog -work work ../src/on_chip/RD53_top.v

vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/clk_wiz_1/clk_wiz_1_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_0/fifo_generator_0_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_2/fifo_generator_2_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/triggerFifo/triggerFifo_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/hitDataFIFO/hitDataFIFO_sim_netlist.vhdl

vlog -work work ../src/perf_counter.v
vlog -work work ../src/sys_top.v
vlog -work work sys_tb.v

vsim -t 1pS -novopt sys_tb -L unisim -L unifast

view signals
view wave

do wave_sys.do

run 20 us

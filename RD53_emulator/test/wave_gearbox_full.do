onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /gearbox_full_tb/clk
add wave -noupdate /gearbox_full_tb/rst_tx
add wave -noupdate /gearbox_full_tb/rst_rx
add wave -noupdate -expand -group {Tx Gearbox} -color {Medium Slate Blue} /gearbox_full_tb/data66
add wave -noupdate -expand -group {Tx Gearbox} -color {Medium Slate Blue} /gearbox_full_tb/data32
add wave -noupdate -expand -group {Tx Gearbox} -color {Medium Slate Blue} /gearbox_full_tb/data_next
add wave -noupdate -expand -group {Tx Gearbox} -color {Medium Slate Blue} /gearbox_full_tb/tx_gb/buffer_132
add wave -noupdate -expand -group {Tx Gearbox} -color {Medium Slate Blue} -radix unsigned /gearbox_full_tb/tx_gb/counter
add wave -noupdate -expand -group {Tx Gearbox} -color {Medium Slate Blue} /gearbox_full_tb/tx_gb/gearbox_en
add wave -noupdate -expand -group {Tx Gearbox} -color {Medium Slate Blue} /gearbox_full_tb/tx_gb/gearbox_rdy
add wave -noupdate -expand -group {Tx Gearbox} -color {Medium Slate Blue} /gearbox_full_tb/tx_gb/upper
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/data32
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/data66_out
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/data_valid
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/buffer_128
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/buffer_pos
add wave -noupdate -expand -group {Rx Gearbox} -color Gold -radix unsigned /gearbox_full_tb/rx_gb/counter
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/data_valid
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/data_valid_i
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/gearbox_en
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/gearbox_rdy
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/rotate
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/left_shift
add wave -noupdate -expand -group {Rx Gearbox} -color Gold /gearbox_full_tb/rx_gb/right_shift
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {8 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 243
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {42 ps}

vlib work

vlog -work work ../src/on_chip/command_process.v
vlog -work work global.v
vlog -work work command_process_tb.v

vsim -t 1ps -novopt command_process_tb

view signals
view wave

do wave_command_process.do

run 15 us

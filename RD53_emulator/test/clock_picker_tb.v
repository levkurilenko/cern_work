`timescale 1ps/1ps

module clock_picker_tb();
   
   parameter halfclk160 = 3125;
   
   reg clk160;
   reg rst;
   reg [1:0] phase_sel;
   wire      clk40;
   
   reg 	     clk40a, clk40b, clk40c, clk40d;
   reg [1:0] ctr;
   
      
   clock_picker dut (
		     .clk160(clk160),
		     .rst(rst),
		     .phase_sel(phase_sel),
		     .clk40(clk40)
		     );

   
   initial begin
      clk160 = 1'b0;
      forever #(halfclk160) clk160 = ~clk160;
   end

   initial begin
      rst = 1'b0;
      phase_sel = 2'b00;
      #(1*halfclk160);
      rst = 1'b1;
      #(8*halfclk160);
      rst = 1'b0;
      #(40*halfclk160);
      phase_sel = 2'b10;
      #(40*halfclk160);
      phase_sel = 2'b01;
      #(40*halfclk160);
      phase_sel = 2'b11;
      #(40*halfclk160);
      $stop;
   end

   initial begin
      #(9*halfclk160) clk40a = 1'b1;
      forever #(4*halfclk160) clk40a = ~clk40a;
   end
   
   initial begin
      #(11*halfclk160) clk40b = 1'b1;
      forever #(4*halfclk160) clk40b = ~clk40b;
   end
   
   initial begin
      #(13*halfclk160) clk40c = 1'b1;
      forever #(4*halfclk160) clk40c = ~clk40c;
   end
   
   initial begin
      #(15*halfclk160) clk40d = 1'b1;
      forever #(4*halfclk160) clk40d = ~clk40d;
   end
   

   
endmodule

// Lev Kurilenko
// Scrambler Descrambler Test
// 5/9/2017

module sd_tb();

integer i = 0;
reg rst = 0;
reg clk = 0;
reg [3:0] data_in = 0;
wire [3:0] data_out;
wire [3:0] data_scram;

sd_test uut (
    .rst(rst),
    .clk(clk),
    .data_in(data_in),
    .data_out(data_out),
    .data_scram(data_scram)
);

initial begin
   forever #5 clk = ~clk;
end

initial begin
    rst = 1;
    #15
    rst = 0;
    
    for (i = 0; i < 16; i++) begin
        data_in = i;
        #10
        assert(data_out == data_in);
    end
    
    $stop;
end

endmodule
